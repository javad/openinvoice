SELECT invoiceNumber,
  DATE (issueDate) AS issueDate,
  CAST (totalAmount AS decimal) AS totalAmount,
  CAST (subtotalAmount AS decimal) AS subtotalAmount,
  CAST (taxAmount AS decimal) AS taxAmount,
  customerId,
  currencyCode,
  (
    SELECT quantity
    FROM Item
    WHERE (Invoice.invoiceNumber = Item.invoiceNumber)
  ) quantity
FROM Invoice WHERE
  currencyCode =  '${currency}' AND
  (issueDate >= DATE('${issueDateFrom}') AND issueDate <= DATE ('${issueDateTo}')) AND
  (totalAmount >= CAST ('${totalFrom}' AS decimal) AND totalAmount <= CAST ('${totalTo}' AS decimal)) AND
  (subtotalAmount >= CAST ('${subtotalFrom}' AS decimal) AND subtotalAmount <= CAST ('${subtotalTo}' AS decimal)) AND
  (taxAmount >= CAST ('${taxFrom}' AS decimal) AND taxAmount <= CAST ('${taxTo}' AS decimal))
ORDER BY ${sortColumn} ${sortOrder};