#!/bin/bash
PRJ_NAME="openinvoice"
PRJ_HOME=$HOME/Repository/${PRJ_NAME}
DB_NAME="${PRJ_NAME}.db"
DB_DIR=$PRJ_HOME/db
SQL_DIR=$PRJ_HOME/src/test/resources/database/data

if [ ! -d "$DB_DIR" ]; then
    echo "$DB_DIR desn't exist."
    exit 1
else
    cd "$DB_DIR"
    if [ -f "$DB_NAME" ]; then
        mv "$DB_NAME" "${DB_NAME}.old"
        echo "Renamed '$DB_NAME'  to '${DB_NAME}.old'"
    fi
fi
echo "Recreating the '$DB_NAME' database ..."

sqlite3 $DB_NAME << EOF

CREATE TABLE BankAccount
(
    name TEXT,
    iban TEXT,
    bic TEXT,
    clearing TEXT,
    otherCode TEXT,
    bankAccountId INTEGER PRIMARY KEY AUTOINCREMENT,
    currencyCode TEXT,
    postalAddressId INT,
    CONSTRAINT postalAddressId_fk FOREIGN KEY (postalAddressId) REFERENCES PostalAddress (postalAddressId)
);
CREATE UNIQUE INDEX BankAccount_iban_uindex ON BankAccount (iban);
CREATE TABLE Contact
(
    telephone TEXT,
    telefax TEXT,
    email TEXT,
    contactId INTEGER PRIMARY KEY AUTOINCREMENT,
    partyId INT,
    name TEXT,
    CONSTRAINT partyId_fk FOREIGN KEY (partyId) REFERENCES Party (partyId)
);
CREATE TABLE Invoice
(
    invoiceId INTEGER PRIMARY KEY AUTOINCREMENT,
    issueDate TEXT,
    invoiceNumber TEXT,
    taxAmount TEXT,
    subtotalAmount TEXT,
    totalAmount TEXT,
    currencyCode TEXT,
    supplierId INT,
    customerId INT,
    payeeBankAccountId INT,
    paymentTermId INT,
    orderIdentifier TEXT,
    CONSTRAINT supplierId_fk FOREIGN KEY (supplierId) REFERENCES Party (partyId),
    CONSTRAINT customerId_fk FOREIGN KEY (customerId) REFERENCES Party (partyId),
    CONSTRAINT payeeBankAccountId_fk FOREIGN KEY (payeeBankAccountId) REFERENCES BankAccount (bankAccountId),
    CONSTRAINT paymentTermId_fk FOREIGN KEY (paymentTermId) REFERENCES PaymentTerm (paymentTermId)
);
CREATE UNIQUE INDEX Invoice_invoiceNumber_uindex ON Invoice (invoiceNumber);
CREATE TABLE Item
(
    name TEXT,
    unit TEXT,
    taxPercentage INT DEFAULT 21 NOT NULL,
    quantity INT,
    taxAmount TEXT,
    subtotalAmount TEXT,
    totalAmount TEXT,
    description TEXT,
    itemId INTEGER PRIMARY KEY AUTOINCREMENT,
    price TEXT,
    currencyCode TEXT,
    invoiceNumber TEXT,
    CONSTRAINT invoiceNumber_fk FOREIGN KEY (invoiceNumber) REFERENCES Invoice (invoiceNumber)
);
CREATE TABLE Party
(
    name TEXT,
    vat TEXT,
    language TEXT,
    website TEXT,
    partyId INTEGER PRIMARY KEY AUTOINCREMENT,
    postalAddressId INT,
    CONSTRAINT postalAddressId_fk FOREIGN KEY (postalAddressId) REFERENCES PostalAddress (postalAddressId)
);
CREATE UNIQUE INDEX Party_vat_uindex ON Party (vat,postalAddressId);
CREATE TABLE PaymentTerm
(
    paymentTermId INTEGER PRIMARY KEY AUTOINCREMENT,
    description TEXT
);
CREATE UNIQUE INDEX PaymentTerm_description_uindex ON PaymentTerm (description);
CREATE TABLE PostalAddress
(
    country TEXT,
    city TEXT,
    postalAddressId INTEGER PRIMARY KEY AUTOINCREMENT,
    street TEXT,
    buildingNumber TEXT,
    postalCode TEXT
);
.read $SQL_DIR/BankAccount.sql
.read $SQL_DIR/Contact.sql
.read $SQL_DIR/Invoice.sql
.read $SQL_DIR/Item.sql
.read $SQL_DIR/Party.sql
.read $SQL_DIR/PaymentTerm.sql
.read $SQL_DIR/PostalAddress.sql
EOF
echo Done
