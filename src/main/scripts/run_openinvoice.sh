#!/bin/bash
#
# Copyright (c) 2018. Plain Source S.P.R.L.
# GNU GPL, see <http://www.gnu.org/licenses/>
#
# A wrapper script to facilitate the invocation of the Open Invoice console.
# It initialises the input file based on the given command-line argument.
# In case of TeX output format, it also runs XeLaTeX to produce the PDF output.
# Visit <http://openinvoice.org> to learn more about Open Invoice.
#
PRJ_NAME="openinvoice"
PRJ_VER="1.1.1"
OI_HOME="${APP_HOME}/$PRJ_NAME-$PRJ_VER"
OI_JAR_FILE="openinvoice-${PRJ_VER}.jar"
OI_JAR_PATH="$OI_HOME/lib/${OI_JAR_FILE}"
INPUT_DIR="${OI_HOME}/resources/requests"
OUTPUT_DIR="${DOC_DIR}/$PRJ_NAME"

headerz "$PRJ_NAME ${PRJ_VER}"
#
# Perform sanity check on Open Invoice home, jar file,
# and the document root.
#
if [ ! -d "$OUTPUT_DIR" ];then
  fatalz "$PRJ_NAME documents directory '$OUTPUT_DIR' doesn't exist"
fi
if [ ! -d "$OI_HOME" ];then
  fatalz "$PRJ_NAME home directory '$OI_HOME' doesn't exist"
fi
if [ ! -f "$OI_JAR_PATH" ];then
  fatalz "$PRJ_NAME jar file '$OI_JAR_PATH' doesn't exist"
fi
if [ ! -d "${INPUT_DIR}" ];then
  fatalz "default input directory '${INPUT_DIR}' doesn't exist"
fi
#
# Checks the command line argument and initialise the default
# input file (to be passed to the Open Invoice) based on the
# command line argument.
#
if [ "$#" != 1 ]; then
  echo "Usage: openinvoice create|clone|render|query|delete"; exit 1
fi
ARG="$1"
IN_FILE=$(cygpath -w "${INPUT_DIR}/${ARG}.xml")

case "$ARG" in
  create|clone|render|delete)
    OUTPUT_DIR=${OUTPUT_DIR}/invoice;;
  query)
    OUTPUT_DIR=${OUTPUT_DIR}/report;;
  *)
    echo "Usage: openinvoice create|clone|render|query|delete"; exit 1;;
  esac
#
# (1) Invokes the Open Invoice console with the default input file (see above),
#     The output file shall be set by Open Invoice by default.
# (2) Upon successful execution of Open Invoice, if the output format is TeX,
#     Open Invoice renders its output in the 'tex' directory, then the XeLaTex
#     is invoked to produces the PDF file in the 'pdf' directory
# (3) Upon successful execution of XeLaTex, clean-up is performed.
#
cd ${OI_HOME}
java -jar lib/${OI_JAR_FILE} -i ${IN_FILE} $ARG
exitCode=$?
if [ "$ARG" != "delete" ]; then
 if [ "$exitCode" == 0 ]; then
  TEX_DIR="${OUTPUT_DIR}/tex"
  PDF_DIR="${OUTPUT_DIR}/pdf"
  if [ ! -d "$PDF_DIR" ]; then
    mkdir "$PDF_DIR"
   fi
  texFilePath=$(find ${TEX_DIR} -name '*.tex' -print | sort -n | tail -1)
  texFilePath=$(cygpath "$texFilePath")
  if [ -f "${texFilePath}" ]; then
      infoz "Compiling `cygpath -w "${texFilePath}"`"
      xelatex --output-dir=$(cygpath ${PDF_DIR}) "${texFilePath}" > /dev/null
      exitCode=$?
      if [ "$exitCode" = 0 ]; then
        cd ${PDF_DIR} && cleanz && rm "${texFilePath}"
      fi
  fi
 fi
fi
headerz "(Exit Code = $exitCode)"

