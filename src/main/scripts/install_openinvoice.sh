#!/usr/bin/env bash
PRJ_NAME="openinvoice"
PRJ_VER="1.1.1"
CFG_FILE=".${PRJ_NAME}"

BASE="$PRJ_NAME-$PRJ_VER"
DIST_DIR="$HOME/Repository/$PRJ_NAME/target"
DIST_FILE="$BASE-bin.tar.gz"
DIST_FILE_PATH=$DIST_DIR/$DIST_FILE

if [ "$#" != 1 ]; then
    echo "Usage: $0 <install dir>"; exit -1
fi
INSTALL_DIR="$1"
if [ -d "$INSTALL_DIR" ]; then
    if [ -f "$DIST_FILE_PATH" ]; then
        if [ -d "$INSTALL_DIR/$BASE" ]; then
            rm -r "$INSTALL_DIR/$BASE"
            echo "removed '$INSTALL_DIR/$BASE' directory."
        fi
        tar xvfz "$DIST_FILE_PATH" -C "$INSTALL_DIR" > /dev/null
        retCode="$?"
        if [ "$retCode" = 0 ]; then
            echo "installed $PRJ_VER version $PRJ_NAME in '$INSTALL_DIR/$BASE'"; exit 0
        fi
    else
        echo "Error: can't find the '$DIST_FILE_PATH' file."; exit -1
    fi
else
    echo "Error: directory'$INSTALL_DIR' doesn't exit."; exit -1
fi

# TODO: to be completed
# mv "$CFG_FILE" $HOME
