/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code Contact} class represents the {@code Party} contact information such as
 * the person's name or telephone number.
 */
public class Contact extends DefaultDatabaseEntity implements DatabaseEntity {

    private long partyId;
    private String name;
    private String telephone;
    private String telefax;
    private String email;


    public enum COLUMNS {name, telephone, telefax, email, partyId}

    public long persistToDatabase() throws SQLException {
        return super.insert(this);
    }

    public String prepareInsertStatement() {
        List<String> valueList = new ArrayList<String>();
        valueList.add(name);
        valueList.add(telephone);
        valueList.add(telefax);
        valueList.add(email);
        valueList.add(String.valueOf(partyId));
        return createInsertSql(getClass().getSimpleName(), EnumSet.range(COLUMNS.name, COLUMNS.partyId), valueList);
    }

    public static List<Contact> findByPartyId(long partyId) throws SQLException {
        String query = "SELECT * FROM Contact WHERE partyId = '" + partyId + "'";
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        List<Contact> contacts = new ArrayList<Contact>();
        while (resultSet.next()) {
            Contact contact = new Contact();
            contact.name = resultSet.getString(COLUMNS.name.toString());
            contact.telefax = resultSet.getString(COLUMNS.telefax.toString());
            contact.telephone = resultSet.getString(COLUMNS.telephone.toString());
            contact.email = resultSet.getString(COLUMNS.email.toString());
            contacts.add(contact);
        }
        return contacts;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getTelefax() {
        return telefax;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setTelefax(String telefax) {
        this.telefax = telefax;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPartyId() {
        return partyId;
    }

    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }
}
