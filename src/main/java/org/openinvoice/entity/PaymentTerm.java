/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.openinvoice.util.Configuration;

import java.sql.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code PaymentTerm} class represents the invoice payment terms and condition.
 */
public class PaymentTerm extends DefaultDatabaseEntity implements DatabaseEntity {

    private static final long DEFAULT_NET_DAYS = 30;
    private long paymentTermId;
    private String description;
    private long netDays;

    public enum COLUMNS {PaymentTermId, description, netDays}

    public PaymentTerm() {
    }

    public PaymentTerm(String description) {
        this.description = description;
        initPaymentTermNetDays();
    }

    /**
     *  Examines the input string to find patterns like '10 Days'. If
     *  found, it extracts the <code>netDays</code> with 10. In case of
     *  parsing error, it sets the net days to the default value of 30.
     */
    private void initPaymentTermNetDays() {
        setNetDays(DEFAULT_NET_DAYS);
        if (description != null && description.contains("Days")) {
           String[] tokens = description.split(" ");
            if (tokens.length > 1) { // otherwise, you won't find the number
                for (int i = 0; i < tokens.length; i++) {
                    String token = tokens[i].trim();
                    if (token.equals("Days")) {
                       token = tokens[i - 1];
                       try {
                           setNetDays(Long.parseLong(token));
                       } catch (NumberFormatException e) {
                           Configuration.globalLogger.error("failed top extract payment net days", e);
                       }
                   }
               }
           }
       }
    }

    public long persistToDatabase() throws SQLException {
        return super.insert(this);
    }

    public String prepareInsertStatement() {
        List<String> valueList = new ArrayList<String>();
        valueList.add(description);
        valueList.add(String.valueOf(netDays));
        return createInsertSql(getClass().getSimpleName(), EnumSet.of(COLUMNS.description, COLUMNS.netDays), valueList);
    }

    public static PaymentTerm findByDescription(String description) throws SQLException {
        String query = "SELECT * FROM PaymentTerm WHERE description = '" + description + "'";
        return executeQuery(query);
    }

    public static PaymentTerm findById(long id) throws SQLException {
        String query = "SELECT * FROM PaymentTerm WHERE paymentTermId = '" + id + "'";
        return executeQuery(query);
    }

    public static long findOrPersist(PaymentTerm paymentTerm) throws SQLException {
        PaymentTerm existingPaymentTerm = PaymentTerm.findByDescription(paymentTerm.getDescription());
        if (existingPaymentTerm != null) {
            return existingPaymentTerm.getPaymentTermId();
        }
        return paymentTerm.persistToDatabase();
    }

    private static PaymentTerm executeQuery(String query) throws SQLException {
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        PaymentTerm paymentTerm = null;
        while (resultSet.next()) {
            paymentTerm = new PaymentTerm();
            paymentTerm.paymentTermId = resultSet.getInt(COLUMNS.PaymentTermId.toString());
            paymentTerm.description = resultSet.getString(COLUMNS.description.toString());
            paymentTerm.netDays = resultSet.getLong(COLUMNS.netDays.toString());
        }
        return paymentTerm;
    }


    public long getPaymentTermId() {
        return paymentTermId;
    }

    public String getDescription() {
        return description;
    }

    public void setPaymentTermId(long paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getNetDays() {
        return netDays;
    }

    public void setNetDays(long netDays) {
        this.netDays = netDays;
    }

}


