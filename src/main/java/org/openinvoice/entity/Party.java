/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code Party} class represents the information related to the invoice
 * {@code Customer} and {@code Supplier}.
 */
public class Party extends DefaultDatabaseEntity implements DatabaseEntity {

    private long partyId;
    private long postalAddressId;
    private String name;
    private String vat;
    private String language;
    private String website;

    private PostalAddress postalAddress;
    private List<Contact> contacts = new ArrayList<Contact>();

    public enum COLUMNS {name, vat, language, website, postalAddressId, partyId}

    public Party() {
    }

    public long persistToDatabase() throws SQLException {
        return super.insert(this);
    }

    public static Party findById(long id) throws SQLException {
        String query = "SELECT * FROM Party WHERE partyId = '" + id + "'";
        return executeQuery(query);
    }

    public static Party findByName(String name) throws SQLException {
        String query = "SELECT * FROM Party WHERE name = '" + name + "'";
        return executeQuery(query);
    }

    public static Party findByNameAndStreetAddress(String name, String givenStreet) throws SQLException {
        Party party = findByName(name);
        if (party != null) {
            PostalAddress postalAddress = party.getPostalAddress();
            if (postalAddress != null) {
                String street = postalAddress.getStreet();
                if (street != null) {
                     if (street.trim().equalsIgnoreCase(givenStreet.trim())) {
                         return party;
                     }
                }
            }
        }
        return null;
    }

    public static long findOrPersist(Party party) throws SQLException {
        if (party.getName() == null || party.getPostalAddress() == null) {
            throw new NullPointerException("valid party must have a name and an address");
        }
        String name = party.getName();
        String street = party.getPostalAddress().getStreet();
        Party existingParty = Party.findByNameAndStreetAddress(name, street);
        if (existingParty != null) {
            return existingParty.getPartyId();
        }
        long postalAddressId = insert(party.getPostalAddress());
        party.setPostalAddressId(postalAddressId);
        long partyId = party.persistToDatabase();
        List<Contact> contactList = party.getContacts();
        if (contactList != null && !contactList.isEmpty()) {
            for (Contact contact : contactList) {
                contact.setPartyId((int) partyId);
                contact.persistToDatabase();
            }
        }
        return partyId;
    }

    public String prepareInsertStatement() {

        List<String> valueList = new ArrayList<String>();
        valueList.add(name);
        valueList.add(vat);
        valueList.add(language);
        valueList.add(website);
        EnumSet columnNameSet =  EnumSet.range(COLUMNS.name, COLUMNS.website);
        if (postalAddressId > 0) {
            columnNameSet.add(COLUMNS.postalAddressId);
            valueList.add(String.valueOf(postalAddressId));
        }
        return createInsertSql(getClass().getSimpleName(),columnNameSet, valueList);
    }

    private static Party executeQuery(String query) throws SQLException {
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        Party party = null;
        while (resultSet.next()) {
            party = new Party();
            party.partyId = resultSet.getInt(COLUMNS.partyId.toString());
            party.postalAddressId = resultSet.getInt(COLUMNS.postalAddressId.toString());
            party.name = resultSet.getString(COLUMNS.name.toString());
            party.vat = resultSet.getString(COLUMNS.vat.toString());
            party.language = resultSet.getString(COLUMNS.language.toString());
            party.website = resultSet.getString(COLUMNS.website.toString());

            party.postalAddress = PostalAddress.findByPostalAddressId(party.postalAddressId);
            party.contacts.addAll(Contact.findByPartyId(party.partyId));
        }
        return party;
    }


    public long getPartyId() {
        return partyId;
    }

    public long getPostalAddressId() {
        return postalAddressId;
    }

    public String getName() {
        return name;
    }

    public String getVat() {
        return vat;
    }

    public String getLanguage() {
        return language;
    }

    public String getWebsite() {
        return website;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public void setPostalAddressId(long postalAddressId) {
        this.postalAddressId = postalAddressId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
