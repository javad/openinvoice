/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import java.sql.SQLException;

/**
 * The {@code DatabaseEntity} interface represents the database persistable entities.
 */
public interface DatabaseEntity {
    public long persistToDatabase() throws SQLException;
}
