/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code PostalAddress} class represents the invoice {@code Customer}
 * and {@code Supplier} address information.
 */
public class PostalAddress extends DefaultDatabaseEntity implements DatabaseEntity {

    private long postalAddressId;
    private String country;
    private String city;
    private String street;
    private String buildingNumber;
    private String postalCode;

    public enum COLUMNS {country, city, street, buildingNumber, postalCode}

    public long persistToDatabase() throws SQLException {
        return super.insert(this);
    }

    public String prepareInsertStatement() {
        List<String> valueList = new ArrayList<String>();
        valueList.add(country);
        valueList.add(city);
        valueList.add(street);
        valueList.add(buildingNumber);
        valueList.add(postalCode);
        return createInsertSql(getClass().getSimpleName(), EnumSet.range(COLUMNS.country  , COLUMNS.postalCode), valueList);
    }

    public static PostalAddress findByPostalAddressId(long postalAddressId) throws SQLException {
        String query = "SELECT * FROM PostalAddress WHERE postalAddressId = '" + postalAddressId + "'";
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        PostalAddress postalAddress = new PostalAddress();
        while (resultSet.next()) {
            postalAddress.postalAddressId = postalAddressId;
            postalAddress.country = resultSet.getString(COLUMNS.country.toString());
            postalAddress.city = resultSet.getString(COLUMNS.city.toString());
            postalAddress.street = resultSet.getString(COLUMNS.street.toString());
            postalAddress.buildingNumber = resultSet.getString(COLUMNS.buildingNumber.toString());
            postalAddress.postalCode = resultSet.getString(COLUMNS.postalCode.toString());
        }
       return postalAddress;
    }

    public long getPostalAddressId() {
        return postalAddressId;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalAddressId(long postalAddressId) {
        this.postalAddressId = postalAddressId;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
