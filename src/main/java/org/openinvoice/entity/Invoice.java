/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.openinvoice.util.ArithmeticUtil;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.DateTimeUtil;


import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code Invoice} class represents the invoice information such as
 * the customer and supplier related information as well as information
 * related to billable items.
 */
public class Invoice extends DefaultDatabaseEntity implements DatabaseEntity {

    public enum COLUMNS {issueDate, dueDate, invoiceNumber, taxAmount, subtotalAmount, totalAmount, currencyCode, supplierId, customerId, payeeBankAccountId, paymentTermId, orderIdentifier, invoiceId}

    private long invoiceId;
    private long supplierId;
    private long customerId;
    private long payeeBankAccountId;
    private long paymentTermId;

    private LocalDate issueDate;
    private LocalDate dueDate;
    private String invoiceNumber;
    private CurrencyUnit currencyCode;
    private String orderIdentifier;

    private Party supplier;
    private Party customer;
    private PaymentTerm paymentTerm;
    private BankAccount payeeBankAccount;

    private Money taxAmount;
    private Money subtotalAmount;
    private Money totalAmount;

    private List<Item> items = new ArrayList<Item>();

    public String createCompositeKey() {
        StringBuilder sb = new StringBuilder();
        if (getIssueDate() != null) {
            sb.append(getIssueDate().format(DateTimeUtil.INTERNAL_DATE_FORMATTER)).append("_");
        }
        if (getInvoiceNumber() != null) {
            sb.append(getInvoiceNumber()).append("_");
        }
        if (getCustomer().getName() != null) {
            sb.append(getCustomer().getName());
        }
        return sb.toString();
    }

    public static void deleteByInvoiceNumber(String invoiceNumber) throws SQLException {
        Invoice invoice = findByInvoiceNumber(invoiceNumber);
        if (invoice == null) {
            throw new SQLException("found no invoice with the invoiceNumber = '" + invoiceNumber + "' to delete");
        }
        Item.deleteByInvoiceNumber(invoiceNumber);

        PreparedStatement deleteStmt = getConnection().prepareStatement("DELETE FROM Invoice WHERE invoiceNumber = ?");
        deleteStmt.setString(1, invoiceNumber);
        deleteStmt.executeUpdate();
    }

    public static Invoice findLatestInvoice() throws SQLException {
        List<Invoice> invoices = findByQuery("SELECT * FROM Invoice ORDER BY CAST (invoiceNumber AS INTEGER) DESC LIMIT 1;");
        if (!invoices.isEmpty()) {
            Invoice latestInvoice = invoices.get(0);
            Configuration.globalLogger.debug("found the latest invoice with invoice ({})", latestInvoice.getInvoiceNumber());
            return latestInvoice;
        } else {
            return null;
        }
    }

    public static Invoice findByInvoiceNumber(String invoiceNumber) throws SQLException {
        List<Invoice> invoices = findByQuery("SELECT * FROM Invoice WHERE invoiceNumber = '" + invoiceNumber + "'");
        return invoices.isEmpty() ? null : invoices.get(invoices.size() - 1);
    }

    public static List<Invoice> findByCustomerId(String customerId) throws SQLException {
        return findByQuery("SELECT * FROM Invoice WHERE customerId = '" + customerId + "'");
    }

    public long persistToDatabase() throws SQLException {
        Invoice existingInvoice = Invoice.findByInvoiceNumber(this.getInvoiceNumber());
        if (existingInvoice != null) {
            throw new SQLException("failed to persist to the database the invoice (" + this.getInvoiceNumber() + "), because it already exist.");
        }
        this.setSupplierId(Party.findOrPersist(this.getSupplier()));
        this.setCustomerId(Party.findOrPersist(this.getCustomer()));
        this.setPayeeBankAccountId(BankAccount.findOrPersist(this.getPayeeBankAccount()));
        this.setPaymentTermId(PaymentTerm.findOrPersist(this.getPaymentTerm()));

        List<Item> itemList = getItems();
        for (Item item : itemList) {
            item.setInvoiceNumber(getInvoiceNumber());
            item.persistToDatabase();
        }
        return insert(this);
    }

    public String prepareInsertStatement() {

        List<String> valueList = new ArrayList<String>();
        valueList.add(issueDate.toString());
        valueList.add(dueDate.toString());
        valueList.add(invoiceNumber.toString());
        valueList.add(taxAmount.getAmount().toString());
        valueList.add(subtotalAmount.getAmount().toString());
        valueList.add(totalAmount.getAmount().toString());
        valueList.add(currencyCode.getCurrencyCode());
        valueList.add(String.valueOf(supplierId));
        valueList.add(String.valueOf(customerId));
        valueList.add(String.valueOf(payeeBankAccountId));
        valueList.add(String.valueOf(paymentTermId));
        StringBuilder insertStatements = new StringBuilder();
        insertStatements.append(createInsertSql(getClass().getSimpleName(), EnumSet.range(COLUMNS.issueDate, COLUMNS.paymentTermId), valueList));
        for (Item item : items) {
            insertStatements.append(item.getInsertStatement(getInvoiceNumber()));
        }
        return insertStatements.toString();
    }

    public void calculateTotals() {
        if (currencyCode == null) {
            throw new NullPointerException("currency can't be null");
        }
        this.subtotalAmount = ArithmeticUtil.createZeroMoney(currencyCode);
        this.taxAmount = ArithmeticUtil.createZeroMoney(currencyCode);
        this.totalAmount = ArithmeticUtil.createZeroMoney(currencyCode);
        for (Item item : items) {
            item.calculateTotals();
            this.subtotalAmount = this.subtotalAmount.plus(item.getSubtotalAmount());
            this.taxAmount = this.taxAmount.plus(item.getTaxAmount());
            this.totalAmount = this.totalAmount.plus(item.getTotalAmount());
        }
    }

    public void calculateDueDate() {
        if (issueDate == null) {
            throw new NullPointerException("invoice issue date can't be null");
        }
        if (this.getDueDate() == null) { // use the net days of payment term to calculate it
            incrementDueDateByPaymentTermNetDays();
        } else {
            if (this.issueDate.isAfter(this.dueDate)) {
                throw new IllegalArgumentException("the invoice issue date '" + issueDate.toString() + "' can't be after the invoice due date '" + dueDate.toString() + "'");
            }
        }
    }

    private static List<Invoice> findByQuery(String query) throws SQLException {
        Configuration.globalLogger.debug("executing SQL query: {}", query);
        Statement statement = getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        List<Invoice> invoices = new ArrayList<Invoice>();

        while (resultSet.next()) {
            Invoice invoice = new Invoice();
            invoice.invoiceId = resultSet.getInt(COLUMNS.invoiceId.toString());
            invoice.supplierId = resultSet.getInt(COLUMNS.supplierId.toString());
            invoice.customerId = resultSet.getInt(COLUMNS.customerId.toString());
            invoice.payeeBankAccountId = resultSet.getInt(COLUMNS.payeeBankAccountId.toString());
            invoice.paymentTermId = resultSet.getInt(COLUMNS.paymentTermId.toString());
            invoice.invoiceNumber = resultSet.getString(COLUMNS.invoiceNumber.toString());
            invoice.currencyCode = CurrencyUnit.of(resultSet.getString(COLUMNS.currencyCode.toString()));
            invoice.issueDate = DateTimeUtil.parse(resultSet.getString(COLUMNS.issueDate.toString()));
            invoice.orderIdentifier = resultSet.getString(COLUMNS.orderIdentifier.toString());
            invoice.supplier = Party.findById(invoice.supplierId);
            invoice.customer = Party.findById(invoice.customerId);
            invoice.paymentTerm = PaymentTerm.findById(invoice.paymentTermId);
            invoice.payeeBankAccount = BankAccount.findByBankAccountId(invoice.payeeBankAccountId).get(0);
            invoice.subtotalAmount =  ArithmeticUtil.createMoney(invoice.currencyCode, resultSet.getString(COLUMNS.subtotalAmount.toString()));
            invoice.taxAmount =  ArithmeticUtil.createMoney(invoice.currencyCode, resultSet.getString(COLUMNS.taxAmount.toString()));
            invoice.totalAmount =  ArithmeticUtil.createMoney(invoice.currencyCode, resultSet.getString(COLUMNS.totalAmount.toString()));
            String dueDateStr = resultSet.getString(COLUMNS.dueDate.toString());
            if (dueDateStr != null) {
                invoice.dueDate = DateTimeUtil.parse(dueDateStr);
            } else { // calculate it based on the net days of the payment term
                invoice.calculateDueDate();
            }
            invoice.items = Item.findByInvoiceNumber(invoice.invoiceNumber);
            invoices.add(invoice);
        }
        return invoices;
    }

    public void incrementInvoiceNumberByOne() {
        if (invoiceNumber != null) {
            int num = Integer.parseInt(invoiceNumber);
            this.invoiceNumber = String.valueOf(++num);
            List<Item> itemList = getItems();
            for (Item item : itemList) {
                item.setInvoiceNumber(this.invoiceNumber);
            }
        }
    }

    public void incrementIssueDateByOneMonth() {
        if (issueDate != null) {
            this.issueDate = issueDate.plusMonths(1);
            this.issueDate = issueDate.withDayOfMonth(issueDate.lengthOfMonth());
        }
    }

    public void incrementDueDateByPaymentTermNetDays() {
        if (issueDate != null) {
            long netDays = getPaymentTerm().getNetDays();
            this.dueDate = issueDate.plusDays(Long.valueOf(netDays).intValue());
        }
    }

    public void reQuantifyItems(int quantity) {
        if (items.isEmpty())
            return;
        for (Item item : items) {
            item.setQuantity(quantity);
        }
    }

    private long getInvoiceId() {
        return invoiceId;
    }

    private long getSupplierId() {
        return supplierId;
    }

    private long getCustomerId() {
        return customerId;
    }

    private long getPayeeBankAccountId() {
        return payeeBankAccountId;
    }

    private long getPaymentTermId() {
        return paymentTermId;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public CurrencyUnit getCurrencyCode() {
        return currencyCode;
    }

    private String getOrderIdentifier() {
        return orderIdentifier;
    }

    public Money getTaxAmount() {
        return taxAmount;
    }

    public Money getSubtotalAmount() {
        return subtotalAmount;
    }

    public Money getTotalAmount() {
        return totalAmount;
    }

    public Party getSupplier() {
        return supplier;
    }

    public Party getCustomer() {
        return customer;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public List<Item> getItems() {
        return items;
    }

    public BankAccount getPayeeBankAccount() {
        return payeeBankAccount;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public void setSupplierId(long supplierId) {
        this.supplierId = supplierId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public void setPayeeBankAccountId(long payeeBankAccountId) {
        this.payeeBankAccountId = payeeBankAccountId;
    }

    public void setPaymentTermId(long paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public void setCurrencyCode(CurrencyUnit currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setOrderIdentifier(String orderIdentifier) {
        this.orderIdentifier = orderIdentifier;
    }

    public void setSupplier(Party supplier) {
        this.supplier = supplier;
    }

    public void setCustomer(Party customer) {
        this.customer = customer;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public void setPayeeBankAccount(BankAccount payeeBankAccount) {
        this.payeeBankAccount = payeeBankAccount;
    }

    public void setTaxAmount(Money taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setSubtotalAmount(Money subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public void setTotalAmount(Money totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }
}
