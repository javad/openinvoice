/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.CurrencyUnit;
import org.openinvoice.util.Configuration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code BankAccount} class represents information related to the bank account.
 * It is used by the supplier {@code Party} class to represent the payee bank
 * account information of the  of the {@code Invoice} class.
*/
 public class BankAccount extends DefaultDatabaseEntity implements DatabaseEntity {

    private long bankAccountId;
    private long postalAddressId;
    private String name;
    private String iban;
    private String bic;
    private String clearing;
    private String otherCode;
    private CurrencyUnit currencyCode;
    private PostalAddress postalAddress;

    /** Defines the database column names for the BankAccount table.*/
    public enum COLUMNS {bankAccountId, postalAddressId, name, iban, bic, clearing, otherCode, currencyCode}

    /**
     * Looks for the given bankAccount and returns its row id. If not found, it persists it in the
     * database and returns its row id.
     * @param bankAccount the bank account to find or persist.
     * @return the database record row identifier of the bank account.
     * @throws SQLException if any SQL or database related issues encountered.
     */
    static long findOrPersist(BankAccount bankAccount) throws SQLException {
        List<BankAccount> existingBankAccountList = BankAccount.findByIban(bankAccount.getIban());
        if (!existingBankAccountList.isEmpty()) {
            return existingBankAccountList.get(0).getBankAccountId();
        }
        PostalAddress postalAddress = bankAccount.getPostalAddress();
        if (postalAddress != null) {
            long postalAddressId = insert(postalAddress);
            bankAccount.setPostalAddressId((int)postalAddressId);
        }
        return bankAccount.persistToDatabase();
    }

    public long persistToDatabase() throws SQLException {
        return insert(this);
    }

    static List<BankAccount> findByBankAccountId(long bankAccountId) throws SQLException {
        return findByQuery("SELECT * FROM BankAccount WHERE bankAccountId = '" + bankAccountId + "'");
    }

    public static List<BankAccount> findByIban(String iban) throws SQLException {
        return findByQuery("SELECT * FROM BankAccount WHERE iban = '" + iban + "'");
    }

    protected String prepareInsertStatement() {
        List<String> valueList = new ArrayList<String>();
        valueList.add(String.valueOf(postalAddressId));
        valueList.add(name);
        valueList.add(iban);
        valueList.add(bic);
        valueList.add(clearing);
        valueList.add(otherCode);
        valueList.add(currencyCode.toString());
        return createInsertSql(getClass().getSimpleName(), EnumSet.range(COLUMNS.postalAddressId, COLUMNS.currencyCode), valueList);
    }

    private static List<BankAccount> findByQuery(String query) throws SQLException {

        Configuration.globalLogger.debug("executing the SQL query: {}", query);

        Statement statement = getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

        while (resultSet.next()) {
            BankAccount bankAccount = new BankAccount();
            bankAccount.bankAccountId = resultSet.getInt(COLUMNS.bankAccountId.toString());
            bankAccount.postalAddressId = resultSet.getInt(COLUMNS.postalAddressId.toString());
            bankAccount.name = resultSet.getString(COLUMNS.name.toString());
            bankAccount.iban = resultSet.getString(COLUMNS.iban.toString());
            bankAccount.bic = resultSet.getString(COLUMNS.bic.toString());
            bankAccount.clearing = resultSet.getString(COLUMNS.clearing.toString());
            bankAccount.otherCode = resultSet.getString(COLUMNS.otherCode.toString());
            bankAccount.currencyCode = CurrencyUnit.of(resultSet.getString(COLUMNS.currencyCode.toString()));
            bankAccount.postalAddress = PostalAddress.findByPostalAddressId(bankAccount.postalAddressId);
            bankAccounts.add(bankAccount);
        }
        return bankAccounts;
    }

    public long getBankAccountId() {
        return bankAccountId;
    }

    public long getPostalAddressId() {
        return postalAddressId;
    }

    public String getName() {
        return name;
    }

    public String getIban() {
        return iban;
    }

    public String getBic() {
        return bic;
    }

    public String getClearing() {
        return clearing;
    }

    public String getOtherCode() {
        return otherCode;
    }

    public CurrencyUnit getCurrencyCode() {
        return currencyCode;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setBankAccountId(long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public void setPostalAddressId(long postalAddressId) {
        this.postalAddressId = postalAddressId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public void setClearing(String clearing) {
        this.clearing = clearing;
    }

    public void setOtherCode(String otherCode) {
        this.otherCode = otherCode;
    }

    public void setCurrencyCode(CurrencyUnit currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }
}
