/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import oasis.names.specification.ubl.schema.xsd.order_2.OrderType;

import org.openinvoice.payload.*;
import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.text.doc.InvoiceDocument;
import org.openinvoice.text.doc.DocumentFactory;
import org.openinvoice.text.doc.ReportDocument;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.DateTimeUtil;
import org.openinvoice.util.payload.PayloadHandler;
import org.openinvoice.util.query.GeneralQuery;
import org.openinvoice.util.exception.OpenInvoiceException;
import org.openinvoice.util.payload.OrderToInvoiceTransformer;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * <code>InvoiceWrapper</code> is a wrapper class exposing the invoice
 * related operations. It provides the following operations:
 * <ul>
 *   <li>createInvoice: create a new invoice based on a UBL order payload.</li>
 *   <li>cloneInvoice: clones a new invoice based on an existing invoice, already stored in the database.</li>
 *   <li>renderInvoice: renders an existing invoice.</li>
 *   <li>queryInvoiceRepository: queries the invoice database and creates a report based on the query result.</li>
 *   <li>deleteInvoice: deletes an existing invoice from the database.</li>
 * </ul>
 */
public class InvoiceWrapper {

    private PayloadHandler payloadHandler = new PayloadHandler();

    /**
     * Created a new invoice based on the UBL order information.
     * @param orderTypeStream contains the UBL {@code OrderType} information
     *
     * @return {@code Invoice}
     * @throws PayloadHandler.PayloadParseException
     * @throws SQLException
     */
    public AbstractDocument createInvoice(InputStream orderTypeStream, OutputStream outputStream, InputStream templateStream) throws OpenInvoiceException, SQLException, IOException{
        OrderType orderType = payloadHandler.unMarshalOrderType(orderTypeStream);
        Invoice invoice = new OrderToInvoiceTransformer(orderType).createInvoice();
        if (Configuration.isDatabasePersistEnabled()) {
            invoice.persistToDatabase();
        }
        InvoiceDocument invoiceDocument = DocumentFactory.getInstance(invoice, Configuration.getInvoiceOutputFormat());
        if (templateStream != null) {
            invoiceDocument.setTemplateStream(templateStream);
        }
        invoiceDocument.render(outputStream);
        return invoiceDocument;
    }

    public AbstractDocument queryInvoiceRepository(InputStream queryTypeInput, OutputStream outputStream, InputStream templateStream) throws OpenInvoiceException, IOException, SQLException {
        QueryType queryType = payloadHandler.unMarshalQueryType(queryTypeInput);
        GeneralQuery query = new GeneralQuery(queryType);
        ReportDocument reportDocument = DocumentFactory.getInstance(AbstractDocument.Format.valueOf(queryType.getOutputFormat().value()));
        if (templateStream != null) {
            reportDocument.setTemplateStream(templateStream);
        }
        reportDocument = query.executeQuery(reportDocument);
        reportDocument.render(outputStream);
        return reportDocument;
    }

    /**
     * Renders an existing invoice.
     *
     * @param renderTypeInput contains {@code invoiceNumber} of the invoice to be rendered. It may also contain {@code outputFormat}.
     * @param outputStream used to write the output of the rendering.
     * @param templateStream the template text containing place holders to be replaced with invoice information.
     * @throws OpenInvoiceException
     * @throws IOException
     */
    public AbstractDocument renderInvoice(InputStream renderTypeInput, OutputStream outputStream, InputStream templateStream) throws OpenInvoiceException, IOException, SQLException {
        RenderType renderType = payloadHandler.unMarshalRenderType(renderTypeInput);
        String invoiceNumber = renderType.getInvoiceNumber();
        String  outputFormatStr = renderType.getOutputFormat().value();

        Invoice invoice = null;
        try {
            invoice = Invoice.findByInvoiceNumber(invoiceNumber);
        } catch (SQLException e) {
            throw new OpenInvoiceException(e);
        }
        if (invoice == null) {
            throw new OpenInvoiceException("can't find an invoice with the invoice number = '" + invoiceNumber + "'");
        }
        AbstractDocument.Format outputFormat = outputFormatStr != null ? AbstractDocument.Format.valueOf(outputFormatStr) : Configuration.getInvoiceOutputFormat();
        InvoiceDocument invoiceDocument = DocumentFactory.getInstance(invoice, outputFormat);
        if (templateStream != null) {
            invoiceDocument.setTemplateStream(templateStream);
        }
        invoiceDocument.render(outputStream);
        return invoiceDocument;
    }

    /**
     * Clones a new invoice based on an existing invoice. If <code>cloneTypeInputStream</code>
     * is null, the latest invoice is used to clone a new invoice.
     *
     * @param cloneTypeInputStream contains optional information needed to clone
     * @throws SQLException
     * @throws OpenInvoiceException
     */
    public AbstractDocument cloneInvoice(InputStream cloneTypeInputStream, OutputStream outputStream, InputStream templateStream) throws SQLException, OpenInvoiceException, IOException {

        String existingInvoiceNumber = null;
        String desiredInvoiceNumber = null;
        String desiredIssueDate = null;
        String desiredItemQuantity = null;
        String outputFormatStr = null;

        if (cloneTypeInputStream != null) {
            CloneType cloneType = payloadHandler.unMarshalCloneType(cloneTypeInputStream);
            existingInvoiceNumber = cloneType.getExistingInvoiceNumber();
            desiredInvoiceNumber = cloneType.getDesieredInvoiceNumber();
            if ( cloneType.getDesiredInvoiceIssueDate() != null)
                desiredIssueDate = cloneType.getDesiredInvoiceIssueDate().toString();

            if (cloneType.getDesiredInvoiceItemQuantity() != null)
                desiredItemQuantity = cloneType.getDesiredInvoiceItemQuantity().getValue().toString();

            if (cloneType.getOutputFormat() != null)
                outputFormatStr = cloneType.getOutputFormat().value();
        }
        Invoice invoice;

        if (existingInvoiceNumber != null) {
            invoice = Invoice.findByInvoiceNumber(existingInvoiceNumber);
            if (invoice == null) {
                throw new OpenInvoiceException("can't find an invoice with the specified invoice number = '" + existingInvoiceNumber + "'");
            }
        } else {  // no existing invoice number specified, find the very latest
            invoice = Invoice.findLatestInvoice();
        }
        if (invoice == null) {
            throw new OpenInvoiceException("couldn't retrieve any invoice to be used for cloning");
        }
        String oldInvoiceNumber = invoice.getInvoiceNumber();
        if (desiredIssueDate != null) {
            LocalDate issueDate = DateTimeUtil.parse(desiredIssueDate);
            invoice.setIssueDate(issueDate);
        } else {
            invoice.incrementIssueDateByOneMonth();
        }
        invoice.incrementDueDateByPaymentTermNetDays();
        if (desiredInvoiceNumber != null) {
            invoice.setInvoiceNumber(String.valueOf(Integer.parseInt(desiredInvoiceNumber)));
        } else {
            invoice.incrementInvoiceNumberByOne();
        }
        if (desiredItemQuantity != null) {
            invoice.reQuantifyItems(Integer.parseInt(desiredItemQuantity));
        }
        invoice.calculateTotals();

        if (Configuration.isDatabasePersistEnabled()) {
            invoice.persistToDatabase();
        }
        AbstractDocument.Format outputFormat = outputFormatStr != null ? AbstractDocument.Format.valueOf(outputFormatStr) : Configuration.getInvoiceOutputFormat();
        InvoiceDocument invoiceDocument = DocumentFactory.getInstance(invoice, outputFormat);
        if (templateStream != null) {
            invoiceDocument.setTemplateStream(templateStream);
        }
        invoiceDocument.render(outputStream);
        return invoiceDocument;
    }

    /**
     * Deletes an existing invoice.
     *
     * @param deleteInputStream contains {@code invoiceNumber} of the invoice to be deleted.
     * @throws OpenInvoiceException
     * @throws IOException
     * @throws SQLException
     */
    public void deleteInvoice(InputStream deleteInputStream) throws OpenInvoiceException, IOException, SQLException {
        DeleteType deleteType = payloadHandler.unMarshalDeleteType(deleteInputStream);
        String invoiceNumber = deleteType.getInvoiceNumber();
        Invoice.deleteByInvoiceNumber(invoiceNumber);
    }
}
