/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.openinvoice.util.Configuration;
import org.openinvoice.util.exception.ConfigurationException;

import java.sql.*;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

/**
 * The {@code DefaultDatabaseEntity} class provides the default implementation for
 * {@code DatabaseEntity} interface. It can be extended by entities which implement
 * the {@code DatabaseEntity} interface.
 */
public abstract class DefaultDatabaseEntity {

    public static Connection getConnection() {
        try {
            return Configuration.getDatabaseConnection();
        } catch (ConfigurationException e) {
           throw new RuntimeException(e);
        }
    }

    protected abstract String prepareInsertStatement();

    protected static String createInsertSql(String tableName, EnumSet columnNameSet, List<String> values) {
      StringBuilder sb = new StringBuilder("INSERT INTO ");
        sb.append(tableName).append(" (");
        int colSize = columnNameSet.size();
        Iterator columnNames = columnNameSet.iterator();
        for (int i = 0; i < colSize; i++) {
            String column = columnNames.next().toString();
            sb.append(column);
            if (i < colSize -1) {
                sb.append(", ");
            }
        }
        sb.append(") VALUES (");
        int valSize =  values.size();
        for (int i = 0; i < valSize; i++) {
            String value = values.get(i);
            sb.append("'").append(value).append("'");
            if (i < valSize - 1) {
                sb.append(", ");
            }
        }
        sb.append(");");
        return sb.toString();
    }

    public static long insert(DefaultDatabaseEntity persistableEntity) throws SQLException {
        PreparedStatement ps =  getConnection().prepareStatement(persistableEntity.prepareInsertStatement(), Statement.RETURN_GENERATED_KEYS);
        try {
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            return rs.getLong(1);
        } finally {
            ps.close();
        }
    }
}
