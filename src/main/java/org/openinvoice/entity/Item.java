/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.openinvoice.util.ArithmeticUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * The {@code Item} class represents the invoice item information such as
 * the item price, quantity and description.
 */
public class Item extends DefaultDatabaseEntity implements DatabaseEntity {

    private long itemId;
    private String name;
    private String unit;
    private String taxPercentage;
    private int quantity;
    private Money price;
    private Money taxAmount;
    private Money subtotalAmount;
    private Money totalAmount;
    private String description;
    private CurrencyUnit currencyUnit;

    private String invoiceNumber;

    public enum COLUMNS {itemId, name, unit, taxPercentage, quantity, price, currencyCode, taxAmount, subtotalAmount, totalAmount, description, invoiceNumber}

    public static int deleteByInvoiceNumber(String invoiceNumber) throws SQLException {
        PreparedStatement deleteStmt = getConnection().prepareStatement("DELETE FROM Item WHERE invoiceNumber = ?");
        deleteStmt.setString(1, invoiceNumber);
        return deleteStmt.executeUpdate();
    }

    public String prepareInsertStatement() {
       return getInsertStatement(invoiceNumber);
    }

    public String getInsertStatement(String invoiceNumber) {
        setInvoiceNumber(invoiceNumber);
        List<String> valueList = new ArrayList<String>();
        valueList.add(name);
        valueList.add(unit);
        valueList.add(String.valueOf(taxPercentage));
        valueList.add(String.valueOf(quantity));
        valueList.add(price.getAmount().toString());
        valueList.add(currencyUnit.getCurrencyCode().toString());
        valueList.add(taxAmount.getAmount().toString());
        valueList.add(subtotalAmount.getAmount().toString());
        valueList.add(totalAmount.getAmount().toString());
        valueList.add(description);
        valueList.add(invoiceNumber);
        return createInsertSql(getClass().getSimpleName(), EnumSet.range(COLUMNS.name, COLUMNS.invoiceNumber), valueList);
    }

    public static Item findByItemId(String itemId) throws SQLException {
        String query = "SELECT * FROM Item WHERE itemId = '" + itemId + "'";
        List<Item> items = executeQuery(query);
        Item item = null;
        if (!items.isEmpty()) {
            item = items.get(0);
        }
        return item;
    }

    public static List<Item> findByInvoiceNumber(String invoiceNumber) throws SQLException {
        String query = "SELECT * FROM Item WHERE invoiceNumber = '" + invoiceNumber + "'";
        return executeQuery(query);
    }

    private static List<Item> executeQuery(String query) throws SQLException {
        ResultSet resultSet = getConnection().createStatement().executeQuery(query);
        List<Item> items = new ArrayList<Item>();
        while(resultSet.next()) {
            Item item = new Item();
            item.invoiceNumber = resultSet.getString(COLUMNS.invoiceNumber.toString());;
            item.name = resultSet.getString(COLUMNS.name.toString());
            item.unit = resultSet.getString(COLUMNS.unit.toString());
            item.taxPercentage = resultSet.getString(COLUMNS.taxPercentage.toString());
            item.quantity = resultSet.getInt(COLUMNS.quantity.toString());
            item.description = resultSet.getString(COLUMNS.description.toString());
            item.currencyUnit =  CurrencyUnit.of(resultSet.getString(COLUMNS.currencyCode.toString()));
            item.price = ArithmeticUtil.createMoney(item.currencyUnit, resultSet.getString(COLUMNS.price.toString()));
            item.subtotalAmount =  ArithmeticUtil.createMoney(item.currencyUnit, resultSet.getString(COLUMNS.subtotalAmount.toString()));
            item.taxAmount = ArithmeticUtil.createMoney(item.currencyUnit, resultSet.getString(COLUMNS.taxAmount.toString()));
            item.totalAmount = ArithmeticUtil.createMoney(item.currencyUnit, resultSet.getString(COLUMNS.totalAmount.toString()));
            items.add(item);
        }
        return items;
    }

    public long persistToDatabase() throws SQLException {
        return super.insert(this);
    }

     public void calculateTotals() {
        this.subtotalAmount =  price.multipliedBy(quantity);
        if (taxPercentage != null) {
            this.taxAmount = ArithmeticUtil.calculateMoneyPercentage(ArithmeticUtil.createBigDecimal(taxPercentage), subtotalAmount);
        }
        this.totalAmount = this.subtotalAmount.plus(taxAmount);
    }

    public String getName() {
        return name;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public int getQuantity() {
        return quantity;
    }

    public Money getPrice() {
        return price;
    }

    public Money getTaxAmount() {
        return taxAmount;
    }

    public Money getSubtotalAmount() {
        return subtotalAmount;
    }

    public Money getTotalAmount() {
        return totalAmount;
    }

    public String getDescription() {
        return description;
    }

    public CurrencyUnit getCurrencyUnit() {
        return currencyUnit;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public void setTaxAmount(Money taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setSubtotalAmount(Money subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public void setTotalAmount(Money totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCurrencyUnit(CurrencyUnit currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
