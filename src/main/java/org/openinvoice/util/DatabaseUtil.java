/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util;

import org.openinvoice.util.exception.ConfigurationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseUtil {

    private static final String  DB_URL_PREFIX = "jdbc:sqlite:";
    private static final String  DB_DRIVER_CLASS = "org.sqlite.JDBC";
    
    public static Connection getDatabaseConnection(String dbFilePath) throws ConfigurationException {
        try {
            Class.forName(DB_DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            throw new ConfigurationException("failed to initialize the JDBC class " + DB_DRIVER_CLASS, e);
        }
        String dbUrlStr = DB_URL_PREFIX + dbFilePath;
        try {
            return DriverManager.getConnection(dbUrlStr);
        } catch (SQLException e) {
            throw new ConfigurationException("failed to connect to the database wih URL " + dbUrlStr, e);
        }
    }
}
