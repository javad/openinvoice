/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util.query;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.joda.money.CurrencyUnit;
import org.openinvoice.entity.DefaultDatabaseEntity;
import org.openinvoice.entity.Invoice;
import org.openinvoice.entity.Party;
import org.openinvoice.payload.DateRangeType;
import org.openinvoice.payload.QueryType;
import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.text.doc.ReportDocument;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.exception.OpenInvoiceException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The <code>GeneralQuery</code> class is a utility class to prepare and execute SQL query statements.
 *
 * Created by jhe on 09/03/2018.
 */
public class GeneralQuery {

    public enum CriteriaTokens {currency, issueDateFrom, issueDateTo, totalFrom, totalTo, subtotalFrom, subtotalTo, taxFrom, taxTo, sortColumn, sortOrder}

    private String queryTemplate;
    private QueryType queryType;

    public GeneralQuery(QueryType queryType) throws IOException {
        this.queryType = queryType;
        String sqlTemplateFilePath =   queryType.getCustomerName() != null?"queries/customer.sql":"queries/general.sql";
        InputStream is = getClass().getClassLoader().getResourceAsStream(sqlTemplateFilePath);
        this.queryTemplate = IOUtils.toString(is);

    }

    public ReportDocument executeQuery(ReportDocument reportDocument) throws SQLException, OpenInvoiceException {

        String defaultAmountFrom = "0.0";
        String defaultAmountTo =   "1000000000";
        String defaultDateFrom =   "1975-01-01";
        String defaultDateTo =     "2975-01-01";
        String defaultSortOrder =  "DESC";
        String defaultSortColumn = Invoice.COLUMNS.issueDate.name();
        String defaultOutputLanguage = Locale.ENGLISH.getLanguage();
        AbstractDocument.Format defaultOutputFormat = AbstractDocument.Format.TEX;

        String subtotalFrom = defaultAmountFrom;
        String subtotalTo = defaultAmountTo;
        if (queryType.getSubtotalAmountRange() != null) {
            subtotalFrom = queryType.getSubtotalAmountRange().getFrom().getValue().toString();
            subtotalTo = queryType.getSubtotalAmountRange().getTo().getValue().toString();
        }

        String taxFrom = defaultAmountFrom;
        String taxTo = defaultAmountTo;
        if (queryType.getTaxAmountRange() != null) {
            taxFrom = queryType.getTaxAmountRange().getFrom().getValue().toString();
            taxTo = queryType.getTaxAmountRange().getTo().getValue().toString();
        }

        String totalFrom = defaultAmountFrom;
        String totalTo = defaultAmountTo;
        if (queryType.getTotalAmountRange() != null) {
            totalFrom = queryType.getTotalAmountRange().getFrom().getValue().toString();
            totalTo = queryType.getTotalAmountRange().getTo().getValue().toString();
        }

        String currencyCode = CurrencyUnit.EUR.getCode();
        if (queryType.getCurrency() != null) {
            currencyCode = queryType.getCurrency().value();
        }

        String issueDateFrom = defaultDateFrom;
        String issueDateTo = defaultDateTo;
        DateRangeType issueDateRange = queryType.getIssueDateRange();
        if (issueDateRange != null) {
            issueDateFrom = LocalDate.parse(issueDateRange.getFrom().toXMLFormat()).toString();
            issueDateTo = LocalDate.parse(issueDateRange.getTo().toXMLFormat()).toString();
        }

        AbstractDocument.Format outputFormat = defaultOutputFormat;
        if (queryType.getOutputFormat() != null) {
            outputFormat = AbstractDocument.Format.valueOf(queryType.getOutputFormat().value());
        }

        String outputLanguageCode = defaultOutputLanguage;
        if (queryType.getOutputLanguage() != null) {
            outputLanguageCode = queryType.getOutputLanguage().value();
        }

        String sortColumn = defaultSortColumn;
        if (queryType.getOutputSortColumn() != null) {
            sortColumn = queryType.getOutputSortColumn().value();
        }

        String sortOrder = defaultSortOrder;
        if (queryType.getOutputSortOrder() != null) {
            sortOrder =  queryType.getOutputSortOrder().value();
        }

        Map<String, String> queryCriteria = new HashMap();
        if (queryType.getCustomerName() != null) {
            Party customer = Party.findByName(queryType.getCustomerName().trim());
            if (customer != null) {
                String customerId = String.valueOf(customer.getPartyId());
                queryCriteria.put("customerId", customerId);
            }
        }
        queryCriteria.put(CriteriaTokens.currency.toString(), currencyCode);
        queryCriteria.put(CriteriaTokens.issueDateFrom.toString(), issueDateFrom);
        queryCriteria.put(CriteriaTokens.issueDateTo.toString(), issueDateTo);
        queryCriteria.put(CriteriaTokens.totalFrom.toString(), totalFrom);
        queryCriteria.put(CriteriaTokens.totalTo.toString(), totalTo);
        queryCriteria.put(CriteriaTokens.subtotalFrom.toString(), subtotalFrom);
        queryCriteria.put(CriteriaTokens.subtotalTo.toString(), subtotalTo);
        queryCriteria.put(CriteriaTokens.taxFrom.toString(), taxFrom);
        queryCriteria.put(CriteriaTokens.taxTo.toString(), taxTo);
        queryCriteria.put(CriteriaTokens.sortColumn.toString(), sortColumn);
        queryCriteria.put(CriteriaTokens.sortOrder.toString(), sortOrder);

        StrSubstitutor queryText = new StrSubstitutor(queryCriteria);
        String query = queryText.replace(queryTemplate);
        Configuration.globalLogger.info("executing query: {}", query);
        ResultSet resultSet = DefaultDatabaseEntity.getConnection().createStatement().executeQuery(query);

        reportDocument.setFormat(outputFormat);
        reportDocument.setOutputLanguageCode(outputLanguageCode);
        reportDocument.setCurrencyUnit(CurrencyUnit.of(currencyCode));
        reportDocument.setResultSet(resultSet);
        reportDocument.setQueryCriteria(queryCriteria);

        return reportDocument;
    }

}
