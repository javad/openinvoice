/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

    private static final String INTERNAL_DATE_PATTERN = "yyyy-MM-dd";
    private static final String INTERNAL_TIME_PATTERN = "hh:mm";

    public static final DateTimeFormatter INTERNAL_DATE_FORMATTER = DateTimeFormatter.ofPattern(INTERNAL_DATE_PATTERN);
    public static final DateTimeFormatter OUTPUT_DATE_FORMATTER = DateTimeFormatter.ofPattern(Configuration.getOutputDatePattern());
    public static final DateTimeFormatter ISO_TIME_FORMATTER = DateTimeFormatter.ofPattern(INTERNAL_TIME_PATTERN);
    public static final DateTimeFormatter ISO_DATE_FORMATTER = DateTimeFormatter.ISO_DATE;;

    public static LocalDate parse(String isoDateStr) {
        return LocalDate.parse(isoDateStr, INTERNAL_DATE_FORMATTER);
    }

    public static LocalDate parse(XMLGregorianCalendar xmlGregorianCalendar) {
        return LocalDate.parse(xmlGregorianCalendar.toXMLFormat());
    }
}
