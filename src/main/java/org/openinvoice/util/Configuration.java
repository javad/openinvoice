/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util;

import org.apache.commons.lang3.SystemUtils;
import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.util.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.util.Properties;

/**
 * Created by jhe on 30/12/2017.
 */
public class Configuration {

    public static final Logger consoleLogger = LoggerFactory.getLogger("ConsoleLogger");
    public static final Logger globalLogger = LoggerFactory.getLogger("GlobalLogger");

    public static final String PRJ_NAME="openinvoice";
    public static final String PRJ_VER="1.1.1";

    private static final String CFG_FILE_NAME = PRJ_NAME + ".properties";
    private static Connection databaseConnection = null;

    private static Properties config = loadConfiguration();

    public static Properties getConfig() {
        return config;
    }

    public static void setConfig(Properties config) {
        Configuration.config = config;
    }

    public static void printConfiguration(PrintStream printStream) throws IOException {
        StringBuilder sb = new StringBuilder(PRJ_NAME).append(" (v.").append(PRJ_VER).append(")");
        config.store(printStream, sb.toString());
    }

    private static File getAppHomeDir() throws ConfigurationException {
        File homeDir = new File(config.getProperty(Key.APP_DIR.getName()));
        if (!homeDir.exists()) {
            throw new ConfigurationException("set the property of the '" + Key.APP_DIR.getName() + "' to a valid directory path");
        }
        return homeDir;
    }

    public static Properties getLocalizedProperties(String languageCode) throws IOException {
        try {
            Properties labels = new Properties();
            labels.load(new FileInputStream(new File(getLabelsDir(), "labels_" + languageCode + ".properties")));
            return labels;
        } catch (ConfigurationException e) {
            throw new IOException(e);
        }
    }

    public static AbstractDocument.Format getInvoiceOutputFormat() {
        return AbstractDocument.Format.valueOf(config.getProperty(Key.INV_OUT_FMT.getName(), AbstractDocument.Format.TEX.name()));
    }

    public static String getOutputDatePattern() {
        return config.getProperty(Key.OUT_DAT_PATT.getName(), "dd-MM-yyyy");
    }

    public static boolean isDatabasePersistEnabled() {
        return Boolean.valueOf(config.getProperty(Key.DB_PERSIST.getName(), "true"));
    }

    public static boolean isInvoiceCurrencyFormattingEnabled() {
        return Boolean.valueOf(config.getProperty(Key.INV_FMT_CUR.getName(), "false"));
    }

    public static boolean isReportCurrencyFormattingEnabled() {
        return Boolean.valueOf(config.getProperty(Key.REP_FMT_CUR.getName(), "false"));
    }


    public static File getInvoiceOutputTemplateFile(AbstractDocument.Format format) throws ConfigurationException {
        return new File(getTemplatesDir(), "invoice/invoice" + format.getSuffix());
    }

    public static File getQueryOutputTemplateFile(AbstractDocument.Format format) throws ConfigurationException {
        return new File(getTemplatesDir(), "report/report" + format.getSuffix());
    }

    public static File getOrCreateInvoiceOutputDir(AbstractDocument.Format format) {
        return getOrCreateOutputDir(Key.INV_OUT_DIR, format);
    }

    public static File getOrCreateReportOutputDir(AbstractDocument.Format format) {
        return getOrCreateOutputDir(Key.REP_OUT_DIR, format);
    }

    public static File getOrCreateOutputDir(Key key, AbstractDocument.Format format) {
        String outDirPath = config.getProperty(key.getName());
        if (outDirPath != null) {
            File outDir = new File(outDirPath, format.name().toLowerCase());
            if (!outDir.exists()) {
                if (!outDir.mkdirs()) {
                    globalLogger.error("failed to create the output directory '{}'", outDir.getPath());
                } else {
                    globalLogger.info("created the output directory '{}'", outDir.getPath());
                }
            }
            return outDir;
        } else {
            return SystemUtils.getUserHome();
        }
    }

    public static void setDatabaseConnection(Connection connection) {
        databaseConnection = connection;
    }

    public static Connection getDatabaseConnection() throws ConfigurationException {
        if (databaseConnection != null) {
            return databaseConnection;
        }
        return DatabaseUtil.getDatabaseConnection(getDatabaseFile().getPath());
    }

    private static Properties loadConfiguration() {
        File configFile = new File(SystemUtils.getUserHome(), "." + CFG_FILE_NAME);
        Properties config = new Properties();
        try {
            if (configFile.exists()) {
                config.load(new FileInputStream(configFile));
                config.setProperty("Config source", configFile.getAbsolutePath());
            } else {
                globalLogger.warn("config source '{}'", configFile.getAbsoluteFile() + " doesn't exist. ");
                config.load(Configuration.class.getClassLoader().getResourceAsStream(CFG_FILE_NAME));
                config.setProperty("Config source", PRJ_NAME + "-" + PRJ_VER + ".jar");
            }
        } catch (IOException e) {
            globalLogger.error("failed to load configuration", e);
            throw new RuntimeException(e); // If no config. found, there's no point of recovery
        }
        return config;
    }

    private static File getFile(Key key) throws ConfigurationException {
        String path = config.getProperty(key.getName());
        if (path == null) {
            throw new ConfigurationException(key.getName() + "can't be null");
        }
        File file = new File(path);
        if (!file.exists()) {
            file = new File(getAppHomeDir(), path);
            consoleLogger.debug("'{}' is set to '{}', but doesn't exist. Going to try '{}'", key.getName(), path, file.getPath());
            if (!file.exists()) {
                throw new ConfigurationException(key.getName() + " must be a valid file path");
            }
        }
        consoleLogger.debug("{} is set to '{}'", key.getName(), file.getAbsolutePath());
        return file;
    }

    private static File getDatabaseFile() throws ConfigurationException {
        return getFile(Key.DB_FILE_PATH);
    }

    private static File getLabelsDir() throws ConfigurationException {
      return getFile(Key.LABELS_DIR);
    }

     private static File getTemplatesDir() throws ConfigurationException {
        return getFile(Key.TEMPLATES_DIR);
    }

    private enum Key {
        APP_DIR("app.dir"),
        DB_PERSIST("database.persist.enabled"),
        DB_FILE_PATH("database.file.path"),
        INV_OUT_FMT("invoice.output.format"),
        OUT_DAT_PATT("output.date.pattern"),
        TEMPLATES_DIR("templates.dir"),
        LABELS_DIR("labels.dir"),
        INV_OUT_DIR("invoice.output.dir"),
        INV_FMT_CUR("invoice.format.currency.enabled"),
        REP_FMT_CUR("report.format.currency.enabled"),
        REP_OUT_DIR("report.output.dir");

        private final String name;

        Key(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
