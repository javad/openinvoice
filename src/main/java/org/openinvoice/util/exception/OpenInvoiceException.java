/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util.exception;

/**
 * Created by jhe on 04/01/2018.
 */
public class OpenInvoiceException extends Exception {

    public OpenInvoiceException() {
    }

    public OpenInvoiceException(Throwable cause) {
        super(cause);
    }

    public OpenInvoiceException(String message) {
        super(message);
    }

    public OpenInvoiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
