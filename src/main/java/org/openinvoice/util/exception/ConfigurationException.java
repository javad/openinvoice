/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util.exception;

import org.openinvoice.util.Configuration;

/**
 * Created by jhe on 22/02/2018.
 */
public class ConfigurationException extends OpenInvoiceException {

    public ConfigurationException() {
    }

    public ConfigurationException(String message) {
        super(message);
        Configuration.globalLogger.error(message, this);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
        Configuration.globalLogger.error(message, cause);
    }

}
