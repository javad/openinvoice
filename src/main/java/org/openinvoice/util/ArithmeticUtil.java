/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by jhe on 11/03/2018.
 */
public class ArithmeticUtil {

    public static final BigDecimal HUNDRED = createBigDecimal("100");
    public static final BigDecimal ZERO = createBigDecimal("0");

    public static BigDecimal createBigDecimal(String amount) {
        return new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal setScale(BigDecimal bd) {
        return bd.setScale(2);
    }

    public static Money createMoney(CurrencyUnit currencyUnit, String amount) {
        return Money.of(currencyUnit, createBigDecimal(amount)) ;
    }

    public static Money createZeroMoney(CurrencyUnit currencyUnit) {
        return Money.zero(currencyUnit);
    }

    public static Money calculateMoneyPercentage(BigDecimal percentage, Money money) {
        BigDecimal rate = percentage.multiply(ArithmeticUtil.HUNDRED);
        return money.multipliedBy(rate, RoundingMode.HALF_UP).dividedBy(ArithmeticUtil.HUNDRED, RoundingMode.HALF_UP);
    }

}
