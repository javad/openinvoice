/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util.payload;

import oasis.names.specification.ubl.schema.xsd.order_2.OrderType;
import org.openinvoice.entity.InvoiceWrapper;
import org.openinvoice.payload.CloneType;
import org.openinvoice.payload.DeleteType;
import org.openinvoice.payload.QueryType;
import org.openinvoice.payload.RenderType;
import org.openinvoice.util.exception.OpenInvoiceException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.InputStream;
import java.io.Writer;

/**
 * Created by jhe on 10/03/2018.
 */
public class PayloadHandler {

    private static final String UBL_ORD_XSD_PATH = "/maindoc/UBL-Order-2.1.xsd";
    private static final String UBL_ORD_PKG_NAME = "oasis.names.specification.ubl.schema.xsd.order_2";

    private static final String OPEN_INV_XSD_PATH = "/xsd/OpenInvoiceTypes.xsd";
    private static final String OPEN_INV_PKG_NAME = "org.openinvoice.payload";

    private static final String DEFAULT_ENCODING = "UTF-8";


    public QueryType unMarshalQueryType(InputStream is) throws PayloadParseException {
        return (QueryType) unMarshalOpenInvoiceType(is);
    }

    public RenderType unMarshalRenderType(InputStream is) throws PayloadParseException {
        return (RenderType) unMarshalOpenInvoiceType(is);
    }

    public CloneType unMarshalCloneType(InputStream is) throws PayloadParseException {
        return (CloneType) unMarshalOpenInvoiceType(is);
    }

    public DeleteType unMarshalDeleteType(InputStream is) throws PayloadParseException {
        return (DeleteType) unMarshalOpenInvoiceType(is);
    }

    public void marshalDeleteType(DeleteType deleteType, Writer writer) throws PayloadParseException {
        marshallOpenInvoiceType(deleteType, writer);
    }

    public void marshalRenderType(RenderType renderType, Writer writer) throws PayloadParseException {
        marshallOpenInvoiceType(renderType, writer);
    }

    public void marshalCloneType(CloneType cloneType, Writer writer) throws PayloadParseException {
        marshallOpenInvoiceType(cloneType, writer);
    }

    public Object unMarshalOpenInvoiceType(InputStream is) throws PayloadParseException {
        try {
            JAXBContext jc = JAXBContext.newInstance(OPEN_INV_PKG_NAME);
            Unmarshaller u = jc.createUnmarshaller();
            Schema schema = getOpenInvoiceSchema();
            if (schema != null) {
                u.setSchema(schema);
            }
            return ((JAXBElement) u.unmarshal(is)).getValue();
        } catch (JAXBException | SAXException e) {
            throw new PayloadParseException(e);
        }
    }

    public void marshallOpenInvoiceType(Object openInvoiceType, Writer writer) throws PayloadParseException {
        try {
            JAXBContext jc = JAXBContext.newInstance(OPEN_INV_PKG_NAME);
            Marshaller m = jc.createMarshaller();
            m.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, DEFAULT_ENCODING);
            m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            Schema schema = getOpenInvoiceSchema();
            if (schema != null) {
                m.setSchema(schema);
            }
            org.openinvoice.payload.ObjectFactory of = new org.openinvoice.payload.ObjectFactory();
            JAXBElement element = null;
            if (openInvoiceType instanceof DeleteType) {
                element = (JAXBElement) of.createDeleteRequest((DeleteType) openInvoiceType);
            } else if (openInvoiceType instanceof CloneType) {
                element = (JAXBElement) of.createCloneRequest((CloneType) openInvoiceType);
            } else if (openInvoiceType instanceof RenderType) {
                element = (JAXBElement) of.createRenderRequest((RenderType) openInvoiceType);
            } else {
                throw new PayloadParseException("invalid request type" + openInvoiceType.getClass().getName());
            }
            m.marshal(element, writer);
        } catch (SAXException | JAXBException e ) {
            throw new PayloadParseException(e);
        }
    }

    public OrderType unMarshalOrderType(InputStream is) throws PayloadParseException {
        try {
            JAXBContext jc = JAXBContext.newInstance(UBL_ORD_PKG_NAME);
            Unmarshaller u = jc.createUnmarshaller();
            Schema schema = getOrderTypeSchema();
            if(schema != null) {
                u.setSchema(schema);
            }
            return (OrderType)((JAXBElement)u.unmarshal(is)).getValue();
        } catch (Exception  e) {
            throw new PayloadParseException(e);
        }
    }

    public Schema getOpenInvoiceSchema() throws SAXException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Class clazz =  InvoiceWrapper.class;
        StreamSource xsdStreamSource = new StreamSource(clazz.getResourceAsStream(OPEN_INV_XSD_PATH),
                clazz.getResource(OPEN_INV_XSD_PATH).toExternalForm());
        return sf.newSchema(xsdStreamSource);
    }

    public Schema getOrderTypeSchema() throws SAXException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Class clazz =  InvoiceWrapper.class;
        StreamSource xsdStreamSource = new StreamSource(clazz.getResourceAsStream(UBL_ORD_XSD_PATH),
                clazz.getResource(UBL_ORD_XSD_PATH).toExternalForm());
        return sf.newSchema(xsdStreamSource);
    }

    public static class PayloadParseException extends OpenInvoiceException {
        public PayloadParseException(String message) {
            super(message);
        }

        public PayloadParseException(Throwable cause) {
            super(cause);
        }
    }

}
