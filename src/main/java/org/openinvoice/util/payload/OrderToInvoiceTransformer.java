/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.util.payload;


import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.*;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.*;
import oasis.names.specification.ubl.schema.xsd.order_2.OrderType;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.openinvoice.entity.*;
import org.openinvoice.util.ArithmeticUtil;
import org.openinvoice.util.DateTimeUtil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhe on 26/01/2018.
 */
public class OrderToInvoiceTransformer {

    private OrderType orderType;
    private Invoice invoice;

    public OrderToInvoiceTransformer(OrderType orderType) {
        this.orderType = orderType;
    }

    public Invoice createInvoice() {
        invoice = new Invoice();
        invoice.setCustomer(createParty(orderType.getBuyerCustomerParty().getParty()));
        invoice.setSupplier(createParty(orderType.getSellerSupplierParty().getParty()));
        invoice.setCurrencyCode(createCurrencyCode());
        invoice.setPayeeBankAccount(createBankAccount());
        invoice.setPaymentTerm(createPaymentTerms());
        invoice.setInvoiceNumber(createInvoiceNumber());
        invoice.setIssueDate(createIssueDate());
        invoice.setDueDate(createDueDate());
        invoice.setCurrencyCode(invoice.getPayeeBankAccount().getCurrencyCode());
        invoice.setItems(createInvoiceItems());

        invoice.calculateTotals();
        invoice.calculateDueDate();

        return invoice;
    }

    private CurrencyUnit createCurrencyCode() {
        DocumentCurrencyCodeType documentCurrencyCodeType = orderType.getDocumentCurrencyCode();
        if (documentCurrencyCodeType != null) {
            String currencyCode = documentCurrencyCodeType.getValue();
            CurrencyUnit.of(currencyCode);
        }
        return null;
    }


    private BankAccount createBankAccount() {
        FinancialAccountType financialAccountType = null;
        List<PaymentMeansType> paymentMeansTypeList =  orderType.getPaymentMeans();
        if (!paymentMeansTypeList.isEmpty()) {
            PaymentMeansType paymentMeansType = paymentMeansTypeList.get(0);
            if (paymentMeansType == null) {
                return null;
            } else {
                financialAccountType = paymentMeansType.getPayeeFinancialAccount();
                if (financialAccountType == null) {
                    return null;
                }
            }
        }
        BankAccount bankAccount = new BankAccount();
        // If no currency is specified at the order level, try to initialize it
        // from the bank account currency
        if (invoice.getCurrencyCode() == null) {
            CurrencyCodeType currencyCodeType = financialAccountType.getCurrencyCode();
            if (currencyCodeType != null) {
                String currencyCode = currencyCodeType.getValue();
                if (currencyCode != null) {
                    bankAccount.setCurrencyCode(CurrencyUnit.of(currencyCode));
                }
            }
        }
        NameType nameType = financialAccountType.getName(); // Bank name
        if (nameType != null) {
            String name = nameType.getValue();
            if (name != null) {
                bankAccount.setName(name);
            }
        }
        IDType idType = financialAccountType.getID(); // bank account number
        if (idType != null) {
            String iban = idType.getValue();
            if (iban != null) {
                bankAccount.setIban(iban);
            }
        }
        BranchType branchType = financialAccountType.getFinancialInstitutionBranch();
        bankAccount.setPostalAddress(createPostalAddress(branchType.getAddress())); // Branch address
        IDType idTypeBic = branchType.getID(); // BIC
        if (idTypeBic != null) {
            String bic = idTypeBic.getValue();
            if (bic != null) {
                bankAccount.setBic(bic);
            }
        }
        return bankAccount;
    }

    private Party createParty(PartyType partyType) {
        Party party = new Party();

        // Name
        List<PartyNameType> partyNamesType = partyType.getPartyName();
        if (!partyNamesType.isEmpty()) {
            PartyNameType partyNameType = partyNamesType.get(0);
            if (partyNameType != null) {
                NameType nameType = partyNameType.getName();
                if (nameType != null) {
                    String name = nameType.getValue();
                    party.setName(name);
                }
            }
        }

        // Address
        AddressType addressType = partyType.getPostalAddress();
        if (addressType != null) {
            party.setPostalAddress(createPostalAddress(addressType));
        }

        // Contact
        Contact contact = createContact(partyType.getContact());
        if (contact != null) {
            party.getContacts().add(contact);
        }

        // Language
        LanguageType languageType = partyType.getLanguage();
        if (languageType != null) {
            LocaleCodeType localeCodeType = languageType.getLocaleCode();
            if (localeCodeType != null) {
                String language = localeCodeType.getValue();
                party.setLanguage(language);
            }
        }

        // Website
        WebsiteURIType websiteURIType = partyType.getWebsiteURI();
        if (websiteURIType != null) {
            String website = websiteURIType.getValue();
            if (website != null) {
                party.setWebsite(website);
            }
        }

        // VAT
        List<PartyTaxSchemeType> partyTaxSchemesType = partyType.getPartyTaxScheme();
        if (!partyTaxSchemesType.isEmpty()) {
            PartyTaxSchemeType partyTaxSchemeType = partyTaxSchemesType.get(0);
            CompanyIDType companyIdType = partyTaxSchemeType.getCompanyID();
            if (companyIdType != null) {
                String vat = companyIdType.getValue();
                if (vat != null) {
                    party.setVat(vat);
                }
            }
        }
        return party;
    }

    private Contact createContact(ContactType contactType) {
        if (contactType == null) return null;
        Contact contact = new Contact();
        NameType nameType = contactType.getName();
        if (nameType != null) {
            String name = nameType.getValue();
            if (name != null) {
                contact.setName(name);
            }
        }
        ElectronicMailType electronicMailType = contactType.getElectronicMail();
        if (electronicMailType != null) {
            String email = electronicMailType.getValue();
            if (email != null) {
                contact.setEmail(email);
            }
        }
        TelephoneType telephoneType = contactType.getTelephone();
        if (telephoneType != null) {
            String tel = telephoneType.getValue();
            if (tel != null) {
                contact.setTelephone(tel);
            }
        }
        TelefaxType telefaxType = contactType.getTelefax();
        if (telefaxType != null) {
            String fax = telefaxType.getValue();
            if (fax != null) {
                contact.setTelefax(fax);
            }
        }
        return contact;
    }

    private PostalAddress createPostalAddress(AddressType addressType) {

        PostalAddress postalAddress = new PostalAddress();
        if (addressType == null) {
            return postalAddress;
        }
        BuildingNumberType buildingNumberType = addressType.getBuildingNumber();
        if (buildingNumberType != null) {
            String buildingNumber = buildingNumberType.getValue();
            if (buildingNumber != null) {
                postalAddress.setBuildingNumber(buildingNumber);
            }
        }
        PostalZoneType postalZoneType = addressType.getPostalZone();
        if (postalZoneType != null) {
            String postalZone = postalZoneType.getValue();
            if (postalZone != null) {
                postalAddress.setPostalCode(postalZone);
            }
        }

        StreetNameType streetNameType = addressType.getStreetName();
        if (streetNameType != null) {
            String street = streetNameType.getValue();
            if (street != null) {
                postalAddress.setStreet(street);
            }
        }

        CityNameType cityNameType = addressType.getCityName();
        if (cityNameType != null) {
            String city = cityNameType.getValue();
            if (city != null) {
                postalAddress.setCity(city);
            }
        }
        CountryType countryType = addressType.getCountry();
        if (countryType != null) {
            IdentificationCodeType identificationCodeType = countryType.getIdentificationCode();
            if (identificationCodeType != null) {
                String country = identificationCodeType.getValue();
                if (country != null) {
                    postalAddress.setCountry(country);
                }
            }
        }
        return postalAddress;
    }
    
    private PaymentTerm createPaymentTerms() {
       List<PaymentTermsType> paymentTermList = orderType.getPaymentTerms();
       if (!paymentTermList.isEmpty()) {
           PaymentTermsType paymentTermsType = paymentTermList.get(0);
           if (paymentTermsType != null) {
               List<NoteType> nodeTypeList = paymentTermsType.getNote();
               if (!nodeTypeList.isEmpty()) {
                   String term = nodeTypeList.get(0).getValue();
                   if (term != null) {
                       return new PaymentTerm(term);
                   }
               }
           }
       }
       return null;
    }

    private String createInvoiceNumber() {
        IDType idType = orderType.getID();
        String invoiceNumber = null;
        if (idType != null) {
            String id = idType.getValue();
            if (!id.isEmpty()) {
                invoiceNumber = id;
            }
        }
        return invoiceNumber;
    }

    private LocalDate createIssueDate() {
        return DateTimeUtil.parse(orderType.getIssueDate().getValue());
    }

    private List<Item> createInvoiceItems() {
        List<OrderLineType> orderLineTypes = orderType.getOrderLine();
        List<Item> items = new ArrayList<Item>();
        for (OrderLineType orderLineType : orderLineTypes) {
            Item item = new Item();

            LineItemType lineItemType = orderLineType.getLineItem();
            InvoiceLineType invoiceLineType = new InvoiceLineType();

            ItemType itemType =  lineItemType.getItem();
            if (itemType != null) {
               List<TaxCategoryType> taxCategoryTypes = itemType.getClassifiedTaxCategory();
               if (!taxCategoryTypes.isEmpty()) {
                   TaxCategoryType taxCategoryType = taxCategoryTypes.get(0);
                   PercentType percentType = taxCategoryType.getPercent();
                   if (percentType != null) {
                       item.setTaxPercentage(percentType.getValue().toPlainString());
                   }
               }
               NameType nameType = itemType.getName();
               if (nameType != null) {
                   String itemName = nameType.getValue();
                   if (itemName != null) {
                       item.setName(itemName);
                   }
               }
            }

            // set description
            DescriptionType descriptionType = lineItemType.getItem().getDescription().get(0);
            if (descriptionType != null) {
                String description = descriptionType.getValue();
                item.setDescription(description);
            }

            // set invoiced quantity
            QuantityType quantityType = lineItemType.getQuantity();
            if (quantityType != null) {
                BigDecimal quantity = ArithmeticUtil.setScale(quantityType.getValue());
                if (quantity != null) {
                    item.setQuantity(quantity.intValue());
                }
                String unitCode = quantityType.getUnitCode();
                if (unitCode != null) {
                    item.setUnit(unitCode);
                }
            }

            // set price
            PriceType priceType = lineItemType.getPrice();
            if (priceType != null) {
                BigDecimal price = ArithmeticUtil.setScale(priceType.getPriceAmount().getValue());
                String currencyCode = priceType.getPriceAmount().getCurrencyID();
                if (price != null && currencyCode != null) {
                    item.setCurrencyUnit(CurrencyUnit.of(currencyCode));
                    item.setPrice(Money.of(item.getCurrencyUnit(), price));
                }
            }
            item.calculateTotals();
            item.setInvoiceNumber(invoice.getInvoiceNumber());
            items.add(item);
        }
        return items;
    }

    public LocalDate createDueDate() {
       List<PaymentMeansType> paymentMeansTypeList =  orderType.getPaymentMeans();
       LocalDate paymentDueDate = null;
       if (!paymentMeansTypeList.isEmpty()) {
           PaymentMeansType paymentMeansType = paymentMeansTypeList.get(0);
           PaymentDueDateType paymentDueDateType = paymentMeansType.getPaymentDueDate();
           if (paymentDueDateType != null) {
               paymentDueDate = DateTimeUtil.parse(paymentDueDateType.getValue());
           }
       }
       return paymentDueDate;
    }

}
