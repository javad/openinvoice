/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.doc;

import org.openinvoice.entity.Invoice;
import org.openinvoice.text.formatter.HtmlFormatter;
import org.openinvoice.text.formatter.TexFormatter;
import org.openinvoice.text.formatter.TextFormatter;
import org.openinvoice.util.Configuration;

import java.io.IOException;
import java.sql.SQLException;

/**
 * A factory utility to ease the creation of <em>Invoice</em> and <em>Report</em> documents.
 *
 * Created by jhe on 29/01/2018.
 */
public class DocumentFactory {

    public static InvoiceDocument getInstance(Invoice invoice, AbstractDocument.Format format) throws IOException, SQLException {
        InvoiceDocument invoiceDocument;
        switch (format) {
            case TEX:
                invoiceDocument = new InvoiceDocument(new TexFormatter(), invoice);
                break;
            case TXT:
                invoiceDocument = new InvoiceDocument(new TextFormatter(), invoice);break;
            case HTML:
                invoiceDocument = new InvoiceDocument(new HtmlFormatter(), invoice);break;
            default:
                throw new IOException("unknown format " + format.toString());
        }
        invoiceDocument.getFormatter().setFormatWithCurrency(Configuration.isInvoiceCurrencyFormattingEnabled());
        return invoiceDocument;
    }

    public static ReportDocument getInstance(AbstractDocument.Format format) throws IOException, SQLException {
        ReportDocument reportDocument;
        switch (format) {
            case TEX:
                reportDocument = new ReportDocument(new TexFormatter());break;
            case TXT:
                reportDocument = new ReportDocument(new TextFormatter());break;
            case HTML:
                reportDocument = new ReportDocument(new HtmlFormatter());break;
            default:
                throw new IOException("unknown format " + format.toString());
        }
        reportDocument.getFormatter().setFormatWithCurrency(Configuration.isReportCurrencyFormattingEnabled());
        return reportDocument;

    }
}
