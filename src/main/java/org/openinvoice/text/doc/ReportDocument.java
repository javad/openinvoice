/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.doc;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.openinvoice.entity.Invoice;
import org.openinvoice.entity.Item;
import org.openinvoice.entity.Party;
import org.openinvoice.text.formatter.AbstractDocumentFormatter;
import org.openinvoice.util.ArithmeticUtil;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.exception.ConfigurationException;
import org.openinvoice.util.query.GeneralQuery;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Formats the invoice entries including the invoice items. To cover the invoices with multiple
 * items, the invoice entry lists the items if there's more than one item.
 *
 * Created by jhe on 08/03/2018.
 */
public class ReportDocument extends AbstractDocument {

    private ResultSet resultSet;
    private CurrencyUnit currencyUnit;
    private String outputLanguageCode;
    private StringBuilder reportContent = new StringBuilder();
    private Money grandSubtotal;
    private Money grandTax;
    private Money grandTotal;
    private BigDecimal grandQuantity;
    private Map<String, String> queryCriteria;


    public ReportDocument(AbstractDocumentFormatter formatter) throws SQLException {
        super(formatter);
    }

    public void render(OutputStream userOutStream) throws IOException, ConfigurationException, SQLException {
        this.formatContent();
        super.render(userOutStream);
    }

    private void updateGrandSubtotal(String amount) {
        this.grandSubtotal = grandSubtotal.plus(ArithmeticUtil.createBigDecimal(amount));
    }

    private void updateGrandTax(String amount) {
        this.grandTax = grandTax.plus(ArithmeticUtil.createBigDecimal(amount));
    }

    private void updateGrandTotal(String amount) {
        this.grandTotal = grandTotal.plus(ArithmeticUtil.createBigDecimal(amount));
    }

    private void updateGrandQuatity(String amount) {
        this.grandQuantity = grandQuantity.add(ArithmeticUtil.createBigDecimal(amount));
    }

   /**
    Formats the invoice entries including the invoice items. To cover the invoices with multiple
    items, the invoice entry lists the items if there's more than one item. The layout of a given
    invoice entry is shown below.

       C1          C2         C3         C4        C5     C6      C7         C8          C9
    Customer | Invoice# | Issue Date | Subtotal | Tax | Total | Item.Qty | Item.Unit | Item.Price
       -     |      -   |      -     |     -    |   - |   -   | Item.Qty | Item.Unit | Item.Price

       The {@code Entry} inner class is used to present an invoice entry. The {@code Entry} inner
       class in turn includes the {@code Details} inner class to present the details of an entry.
     */
    private String formatContent() throws SQLException {

        this.grandQuantity = ArithmeticUtil.ZERO;
        this.grandSubtotal = ArithmeticUtil.createZeroMoney(currencyUnit);
        this.grandTax = ArithmeticUtil.createZeroMoney(currencyUnit);
        this.grandTotal = ArithmeticUtil.createZeroMoney(currencyUnit);

        int rowCount = 1;
        while (resultSet.next()) {
            Entry entry = new Entry(getFormatter(), 7);

            String countCell = getFormatter().formatTableCell(String.valueOf(rowCount));
            entry.setIndex(countCell);

            // (1) Customer
            long customerId = resultSet.getLong(Invoice.COLUMNS.customerId.toString());
            Party customer = Party.findById(customerId);
            String customerCell = getFormatter().formatTableCell(customer == null? String.valueOf(customerId): customer.getName().trim());
            entry.setCustomer(customerCell);

            // (2) Invoice number
            String invoiceNumberStr = resultSet.getString(Invoice.COLUMNS.invoiceNumber.toString());
            String invoiceNumberCell = getFormatter().formatTableCell(invoiceNumberStr);
            entry.setInvoiceNumber(invoiceNumberCell);

            // (3) Issue Date
            String issueDateCell = getFormatter().formatTableCell(resultSet.getString(Invoice.COLUMNS.issueDate.toString()));
            entry.setIssueDate(issueDateCell);

            // (4) Subtotal
            String subtotalStr = resultSet.getString(Invoice.COLUMNS.subtotalAmount.toString());
            updateGrandSubtotal(subtotalStr);
            String subtotalCell = getFormatter().formatTableCellAsMoney(subtotalStr, currencyUnit);
            entry.setSubtotal(subtotalCell);

            // (5) Tax
            String taxStr = resultSet.getString(Invoice.COLUMNS.taxAmount.toString());
            updateGrandTax(taxStr);
            String taxCell = getFormatter().formatTableCellAsMoney(taxStr, currencyUnit);
            entry.setTax(taxCell);

            // (6) Total - Special behaviour for the last cell
            String totalStr = resultSet.getString(Invoice.COLUMNS.totalAmount.toString());
            Money totalMoney = Money.of(currencyUnit,ArithmeticUtil.createBigDecimal(totalStr));
            updateGrandTotal(totalStr);
            String totalCell = getFormatter().formatTableCell(totalMoney);
            entry.setTotal(totalCell);

            // (7) Item quantity and unit
            List<Item> itemList = Item.findByInvoiceNumber(String.valueOf(invoiceNumberStr));
            List<String> subList = new ArrayList<String>();
            for (Item item: itemList) {
                if (item != null) {
                    String quantityStr = String.valueOf(item.getQuantity());
                    updateGrandQuatity(quantityStr);
                    String unitStr = item.getUnit();
                    String priceStr = getFormatter().formatMoney(item.getPrice());
                   Entry.Details details =  new Entry.Details(getFormatter().formatTableCell(quantityStr),
                           getFormatter().formatTableCell(unitStr),
                           getFormatter().formatLastTableCell(priceStr));
                   entry.getDetailsList().add(details);
                }
            }
            reportContent.append(entry.toString());
            rowCount++;
        }
        Configuration.globalLogger.info("Found {} records", rowCount);
        return reportContent.toString();
    }

    @Override
    public void initTokens() throws IOException {

         setLocalizedProperties(Configuration.getLocalizedProperties(outputLanguageCode));

        // -------------------------------------------------------------------
        // The main invoice table labels
        // -------------------------------------------------------------------
        addLocalizedToken(InvoiceDocument.Keys.inv_issue_date_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.customer_name_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_id_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_issue_date_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.customer_id_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_item_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_item_quantity_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_item_unit_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_item_price_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_item_subtotal_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_name_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_subtotal_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_tax_total_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_total_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.inv_currency_code_label.toString());

        addDynamicToken(InvoiceDocument.Keys.inv_currency_code.toString(),
                getFormatter().escapeText((currencyUnit.getCurrencyCode())));

        // -------------------------------------------------------------------
        // The m    ain invoice table content
        // -------------------------------------------------------------------
        addDynamicToken(InvoiceDocument.Keys.inv_items.toString(), reportContent.toString());

        // -------------------------------------------------------------------
        // Summary of total
        // -------------------------------------------------------------------
        addDynamicToken(Keys.rep_grand_subtotal.toString(),
                getFormatter().getMoneyFormatter().print(grandSubtotal));
        addDynamicToken(Keys.rep_grand_tax.toString(),
                getFormatter().getMoneyFormatter().print(grandTax));
        addDynamicToken(Keys.rep_grand_total.toString(),
                getFormatter().getMoneyFormatter().print(grandTotal));
        addDynamicToken(Keys.rep_grand_quantity.toString(), grandQuantity.toString());
        addLocalizedToken(Keys.rep_grand_subtotal_label.toString());
        addLocalizedToken(Keys.rep_grand_tax_label.toString());
        addLocalizedToken(Keys.rep_grand_total_label.toString());
        addLocalizedToken(Keys.rep_grand_quantity_label.toString());

        // -------------------------------------------------------------------
        // Document Metadata
        // -------------------------------------------------------------------
        addDynamicToken(Keys.rep_name.toString(),  Configuration.PRJ_NAME.toUpperCase());
        addDynamicToken(InvoiceDocument.Keys.doc_name.toString(),createDefaultDocumentName());
        addLocalizedToken(InvoiceDocument.Keys.doc_creation_date_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.doc_creation_time_label.toString());
        addDynamicToken(InvoiceDocument.Keys.doc_creation_date.toString(), getMetaData().getCreationDate());
        addDynamicToken(InvoiceDocument.Keys.doc_creation_time.toString(), getMetaData().getCreationTime());
        addLocalizedToken(InvoiceDocument.Keys.doc_name_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.doc_creation_date_label.toString());
        addLocalizedToken(InvoiceDocument.Keys.doc_creation_time_label.toString());
        addLocalizedToken(Keys.rep_name_label.toString());

        // -------------------------------------------------------------------
        // Query Criteria
        // -------------------------------------------------------------------
        addLocalizedToken(Keys.rep_query_issue_date_range_label.toString());
        addDynamicToken(Keys.rep_query_issue_date_from.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.issueDateFrom.toString()));
        addDynamicToken(Keys.rep_query_issue_date_to.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.issueDateTo.toString()));
        addLocalizedToken(Keys.rep_query_total_range_label.toString());
        addDynamicToken(Keys.rep_query_total_from.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.totalFrom.toString()));
        addDynamicToken(Keys.rep_query_total_to.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.totalTo.toString()));
        addLocalizedToken(Keys.rep_query_subtotal_range_label.toString());
        addDynamicToken(Keys.rep_query_subtotal_from.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.subtotalFrom.toString()));
        addDynamicToken(Keys.rep_query_subtotal_to.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.subtotalTo.toString()));
        addLocalizedToken(Keys.rep_query_tax_range_label.toString());
        addDynamicToken(Keys.rep_query_tax_from.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.taxFrom.toString()));
        addDynamicToken(Keys.rep_query_tax_to.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.taxTo.toString()));
        addLocalizedToken(Keys.rep_query_currency_code_label.toString());
        addDynamicToken(Keys.rep_query_currency_code.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.currency.toString()));
        addLocalizedToken(Keys.rep_query_sort_column_label.toString());
        addDynamicToken(Keys.rep_query_sort_column.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.sortColumn.toString()));
        addLocalizedToken(Keys.rep_query_sort_order_label.toString());
        addDynamicToken(Keys.rep_query_sort_order.toString(),
                queryCriteria.get(GeneralQuery.CriteriaTokens.sortOrder.toString()));
    }

    /**
     * Created the report document file name based on the invoice period. The file name has the
     * following format: OPENINVOICE_Report_YYYY-MM-DD_YYYY-MM-DD.tex
     */
    public String createDefaultDocumentName() {
        char SEP = '_';
        StringBuffer sb = new StringBuffer();
        sb.append(Configuration.PRJ_NAME).append(SEP);
        if (getTokenMap() != null) {
            Object reportName = getTokenMap().get(Keys.rep_name_label);
            if (reportName != null) {
                sb.append(SEP).append(reportName);
            }
        }
        sb.append(getQueryCriteria().getOrDefault(GeneralQuery.CriteriaTokens.issueDateFrom.toString(),
                getMetaData().getCreationDate()));
        sb.append(SEP).append(getQueryCriteria().getOrDefault(GeneralQuery.CriteriaTokens.issueDateTo.toString(),
                getMetaData().getCreationDate()));
        if (getFormat() != null) {
            sb.append(getFormat().getSuffix());
        } else {
            throw new NullPointerException("can't figure out the report file name suffix");
        }
        setDocumentName(sb.toString());
        return sb.toString();
    }
    
    @Override
    public File getDefaultTemplateFile() throws ConfigurationException {
        return Configuration.getQueryOutputTemplateFile(getFormat());
    }

    @Override
    public File getDefaultOutputFile() {
        return new File(Configuration.getOrCreateReportOutputDir(getFormat()), createDefaultDocumentName());
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public CurrencyUnit getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(CurrencyUnit currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getOutputLanguageCode() {
        return outputLanguageCode;
    }

    public void setOutputLanguageCode(String outputLanguageCode) {
        this.outputLanguageCode = outputLanguageCode;
    }

    public Map getQueryCriteria() {
        return queryCriteria;
    }

    public void setQueryCriteria(Map queryCriteria) {
        this.queryCriteria = queryCriteria;
    }

    // -------------------------------------------------------------------
    // Inner class
    // -------------------------------------------------------------------

    /**
     *  The {@code Entry} inner class is used to present an invoice entry. The {@code Entry} inner
     *  class in turn includes the {@code Details} inner class to present the details of an entry.
     */
    static class Entry {

        int mainCellCount = 7;

        private AbstractDocumentFormatter formatter;
        private String index;
        private String customer;
        private String invoiceNumber;
        private String issueDate;
        private String subtotal;
        private String tax;
        private String total;

        private List<Details> detailsList = new ArrayList<Details>();

        public Entry(AbstractDocumentFormatter formatter, int mainCellCount) {
            this.formatter = formatter;
            this.mainCellCount = mainCellCount;
        }

        public String createEmptyLine() {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < mainCellCount; j++) {
                sb.append(formatter.formatEmptyTableCell());
            }
            return sb.toString();
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public void setInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
        }

        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }

        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<Details> getDetailsList() {
            return detailsList;
        }

        public String toString() {

            StringBuilder sb = new StringBuilder();
            StringBuilder mainLine = new StringBuilder();

            mainLine.append(index);
            mainLine.append(customer);
            mainLine.append(invoiceNumber);
            mainLine.append(issueDate);
            mainLine.append(subtotal);
            mainLine.append(tax);
            mainLine.append(total);

            if (detailsList.isEmpty()) {
                throw new IllegalArgumentException("can't produce report for an invoice with no details");
            }
            sb.append(formatter.formatTableRow(mainLine.toString() + detailsList.get(0)));
            if (detailsList.size() > 1) { // the invoice has multiple items
                String emptyLine = createEmptyLine();
                for (int i = 1; i < detailsList.size(); i++) {
                    sb.append(formatter.formatTableRow(emptyLine.toString() + detailsList.get(i)));
                }
            }
            return sb.toString();
        }

        static class Details {

            private String quantity;
            private String unit;
            private String price;

            public Details(String quantity, String unit, String price) {
                this.quantity = quantity;
                this.unit = unit;
                this.price = price;
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append(quantity);
                sb.append(unit);
                sb.append(price);
                return sb.toString();
            }

        }
    }

    public enum Keys {
        rep_name_label,
        rep_grand_subtotal_label,
        rep_grand_tax_label,
        rep_grand_total_label,
        rep_grand_quantity_label,
        rep_name,
        rep_grand_subtotal,
        rep_grand_tax,
        rep_grand_total,
        rep_grand_quantity,
        rep_query_issue_date_range_label, rep_query_issue_date_from, rep_query_issue_date_to,
        rep_query_total_range_label, rep_query_total_from, rep_query_total_to,
        rep_query_subtotal_range_label, rep_query_subtotal_from, rep_query_subtotal_to,
        rep_query_tax_range_label, rep_query_tax_from, rep_query_tax_to,
        rep_query_currency_code_label, rep_query_currency_code,
        rep_query_sort_column_label, rep_query_sort_column,
        rep_query_sort_order_label, rep_query_sort_order
    }
}
