/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.doc;

import org.joda.money.CurrencyUnit;
import org.openinvoice.entity.*;
import org.openinvoice.text.formatter.AbstractDocumentFormatter;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.exception.ConfigurationException;
import org.openinvoice.util.DateTimeUtil;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

/**
 * Formats and presents the information related to the invoice and its entries.
 *
 * Created by jhe on 30/12/2017.
 */
public class InvoiceDocument extends AbstractDocument {

    private Invoice invoice;

    public InvoiceDocument(AbstractDocumentFormatter formatter, Invoice invoice) throws SQLException {
        super(formatter);
        setInvoice(invoice);
    }

    public File getDefaultTemplateFile() throws ConfigurationException {
        return Configuration.getInvoiceOutputTemplateFile(getFormat());
    }

    public File getDefaultOutputFile() throws ConfigurationException {
        return new File(Configuration.getOrCreateInvoiceOutputDir(getFormat()), createDefaultDocumentName());
    }


    public String createDefaultDocumentName() {
        return invoice.createCompositeKey() +  getFormat().getSuffix();
    }

    private String formatContent()  {
        List<Item> items = invoice.getItems();
        StringBuilder itemsText = new StringBuilder();
        for (Item item : items) {
            itemsText.append(formatItem(item));
        }
        return itemsText.toString();
    }

    public String formatItem(Item item) {
        StringBuilder itemText = new StringBuilder();
        itemText.append(getFormatter().formatTableCell(item.getDescription()));
        itemText.append(getFormatter().formatTableCell(item.getQuantity() + " " + item.getUnit()));
        itemText.append(getFormatter().formatTableCell(item.getPrice()));
        itemText.append(getFormatter().formatTableCell(item.getSubtotalAmount()));
        itemText.append(getFormatter().formatTableCell(item.getTaxAmount()));
        itemText.append(getFormatter().formatLastTableCell(item.getTotalAmount()));
        return getFormatter().formatTableRow(itemText.toString());
    }

    public String formatAddress(PostalAddress address) {
        String addressText = address.getStreet() + " " +
                address.getBuildingNumber() + " " +
                address.getPostalCode() + " " +
                address.getCity() + " " +
                address.getCountry();
        return getFormatter().escapeText(addressText);
    }


    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public void initTokens() throws IOException {

        String customerLanguage = invoice.getCustomer().getLanguage();
        setLocalizedProperties(Configuration.getLocalizedProperties(customerLanguage == null? "en": customerLanguage));
        // -------------------------------------------------------------------
        // Invoice Labels
        // -------------------------------------------------------------------
        addLocalizedToken(Keys.inv_issue_date_label.toString());
        addLocalizedToken(Keys.supplier_tel_label.toString());
        addLocalizedToken(Keys.supplier_fax_label.toString());
        addLocalizedToken(Keys.supplier_website_url_label.toString());
        addLocalizedToken(Keys.inv_payment_terms_label.toString());
        addLocalizedToken(Keys.supplier_name_label.toString());
        addLocalizedToken(Keys.customer_name_label.toString());
        addLocalizedToken(Keys.order_id_label.toString());
        addLocalizedToken(Keys.inv_id_label.toString());
        addLocalizedToken(Keys.inv_issue_date_label.toString());
        addLocalizedToken(Keys.inv_due_date_label.toString());
        addLocalizedToken(Keys.customer_id_label.toString());
        addLocalizedToken(Keys.supplier_bank_name_label.toString());
        addLocalizedToken(Keys.inv_item_label.toString());
        addLocalizedToken(Keys.inv_item_description_label.toString());
        addLocalizedToken(Keys.inv_item_quantity_label.toString());
        addLocalizedToken(Keys.inv_item_price_label.toString());
        addLocalizedToken(Keys.inv_item_subtotal_label.toString());
        addLocalizedToken(Keys.inv_item_vat_label.toString());
        addLocalizedToken(Keys.inv_item_total_label.toString());
        addLocalizedToken(Keys.inv_name_label.toString());
        addLocalizedToken(Keys.supplier_bank_account_label.toString());
        addLocalizedToken(Keys.supplier_bank_bic_label.toString());
        addLocalizedToken(Keys.supplier_vat_label.toString());
        addLocalizedToken(Keys.customer_vat_label.toString());
        addLocalizedToken(Keys.customer_address_label.toString());
        addLocalizedToken(Keys.supplier_address_label.toString());
        addLocalizedToken(Keys.supplier_bank_address_label.toString());
        addLocalizedToken(Keys.inv_subtotal_label.toString());
        addLocalizedToken(Keys.inv_tax_total_label.toString());
        addLocalizedToken(Keys.inv_total_label.toString());
        addLocalizedToken(Keys.inv_payment_terms_label.toString());
        // -------------------------------------------------------------------
        // Main Invoice values
        // -------------------------------------------------------------------
        addDynamicToken(Keys.inv_issue_date.toString(),
                invoice.getIssueDate().format(DateTimeUtil.OUTPUT_DATE_FORMATTER));
        addDynamicToken(Keys.inv_due_date.toString(),
                invoice.getDueDate() == null? "" : invoice.getDueDate().format(DateTimeUtil.OUTPUT_DATE_FORMATTER));
        addDynamicToken(Keys.inv_id.toString(), invoice.getInvoiceNumber());

        addDynamicToken(Keys.inv_subtotal.toString(),
                getFormatter().getMoneyFormatter().print(invoice.getSubtotalAmount()));
        addDynamicToken(Keys.inv_tax_total.toString(),
                getFormatter().getMoneyFormatter().print(invoice.getTaxAmount()));
        addDynamicToken(Keys.inv_total.toString(),
                getFormatter().getMoneyFormatter().print(invoice.getTotalAmount()));

        addDynamicToken(Keys.inv_items.toString(), formatContent());

        PaymentTerm paymentTerm = invoice.getPaymentTerm();
        if (paymentTerm != null) {
            addDynamicToken(Keys.inv_payment_terms.toString(), paymentTerm.getDescription());
        }
        // -------------------------------------------------------------------
        // Supplier related values
        // -------------------------------------------------------------------
        addDynamicToken(Keys.supplier_name.toString(), invoice.getSupplier().getName());
        PostalAddress supplierAddress = invoice.getSupplier().getPostalAddress();
        if (supplierAddress != null) {
            addDynamicToken(Keys.supplier_address.toString(), formatAddress(supplierAddress));
        }
        List<Contact> supplierContacts = invoice.getSupplier().getContacts();
        if (supplierContacts != null && !supplierContacts.isEmpty()) {
            Contact contact = invoice.getSupplier().getContacts().get(0);
            addDynamicToken(Keys.supplier_tel.toString(), contact.getTelephone() != null?contact.getTelephone():"");
            addDynamicToken(Keys.supplier_fax.toString(), contact.getTelefax() != null?contact.getTelefax():"");
        }
        String website = invoice.getSupplier().getWebsite();
        if (website != null) {
            addDynamicToken(Keys.supplier_website_url.toString(), invoice.getSupplier().getWebsite());
        }
        BankAccount bankAccount = invoice.getPayeeBankAccount();
        if (bankAccount != null) {
            addDynamicToken(Keys.supplier_bank_name.toString(), bankAccount.getName());
            addDynamicToken(Keys.supplier_bank_account.toString(), bankAccount.getIban());
            addDynamicToken(Keys.supplier_bank_bic.toString(), bankAccount.getBic());
            PostalAddress bankAddress = bankAccount.getPostalAddress();
            if (bankAddress != null) {
                addDynamicToken(Keys.supplier_bank_address.toString(), formatAddress(bankAddress));
            }
        }
        String vat = invoice.getSupplier().getVat();
        if (vat != null) {
            addDynamicToken(Keys.supplier_vat.toString(), vat);
        }
        // -------------------------------------------------------------------
        // Customer related values
        // -------------------------------------------------------------------
        getTokenMap().put(Keys.customer_name.toString(), invoice.getCustomer().getName());
        getTokenMap().put(Keys.customer_id.toString(), invoice.getCustomer().getName());

        PostalAddress customerAddress = invoice.getCustomer().getPostalAddress();
        if (customerAddress != null) {
            getTokenMap().put(Keys.customer_address.toString(), formatAddress(customerAddress));
        }
        String customerVat = invoice.getCustomer().getVat();
        if (customerVat != null) {
            getTokenMap().put(Keys.customer_vat.toString(), customerVat);
        }
        CurrencyUnit invoiceCurrency = invoice.getCurrencyCode();
        if (invoiceCurrency != null) {
            getTokenMap().put(Keys.inv_currency_code.toString(), invoiceCurrency.getCode());
        }
    }

    public enum Keys {
        inv_name_label,
        supplier_bank_name_label,
        supplier_bank_account_label,
        supplier_bank_bic_label,
        supplier_vat_label,
        order_id_label,
        inv_item_label,
        inv_item_description_label,
        inv_item_quantity_label,
        inv_item_unit_label,
        inv_item_price_label,
        inv_item_subtotal_label,
        inv_item_vat_label,
        inv_item_total_label,
        inv_total_label,
        inv_subtotal_label,
        inv_tax_total_label,
        inv_issue_date_label,
        inv_due_date_label,
        supplier_tel_label,
        supplier_fax_label,
        supplier_website_url_label,
        inv_payment_terms_label,
        supplier_name_label,
        supplier_address_label,
        customer_address_label,
        supplier_bank_address_label,
        customer_name_label,
        customer_id_label,
        inv_id_label,
        customer_vat_label,
        inv_currency_code_label,
        doc_creation_date_label,
        doc_creation_time_label,
        doc_name_label,

        inv_issue_date,
        inv_due_date,
        inv_payment_terms,
        supplier_name,
        supplier_address,
        supplier_tel,
        supplier_fax,
        supplier_website_url,
        supplier_bank_name,
        supplier_bank_account,
        supplier_bank_bic,
        supplier_bank_address,
        supplier_vat,
        customer_name,
        customer_address,
        customer_id,
        customer_vat,
        inv_id,
        inv_currency_code,
        inv_subtotal,
        inv_tax_total,
        inv_total,
        inv_items,
        doc_creation_date,
        doc_creation_time,
        doc_name,
    }

}
