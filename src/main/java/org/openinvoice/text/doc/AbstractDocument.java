/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.doc;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.openinvoice.text.formatter.AbstractDocumentFormatter;
import org.openinvoice.util.exception.ConfigurationException;
import org.openinvoice.util.DateTimeUtil;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * Serves as the base class for documents such as <code>InvoiceDocument</code> and <code>ReportDocument</code>
 * classes.
 *
 * Created by jhe on 08/03/2018.
 */
public abstract class AbstractDocument {

    private String documentName;
    private Format format;
    private AbstractDocumentFormatter formatter;
    private String templateText;
    private OutputStream outputStream;
    private InputStream templateStream;
    private Map tokenMap = new Hashtable();
    private MetaData metaData = new MetaData();
    private Properties localizedProperties = new Properties();

    public AbstractDocument() {}

    public AbstractDocument(AbstractDocumentFormatter formatter) throws SQLException {
        this.metaData = new MetaData();
        setFormatter(formatter);
        setFormat(formatter.getFormat());
    }

    public MetaData getMetaData() {
         return metaData;
    }

    public void addMetaData(MetaData.key key, String value) {
        metaData.addData(key, value);
    }

    public String createDefaultDocumentName() {
        StringBuffer sb = new StringBuffer();
        sb.append(getMetaData().getCreationDate());
        if (format != null) {
            sb.append(format.getSuffix());
        }
        this.documentName = sb.toString();
        return sb.toString();
    }

    public void render(OutputStream userOutStream) throws IOException, ConfigurationException, SQLException {
        initTokens();
        if (userOutStream == null) {
            setOutputStream(new FileOutputStream(getDefaultOutputFile()));
            addMetaData(MetaData.key.output, getDefaultOutputFile().getAbsolutePath());
        } else {
            setOutputStream(userOutStream);
        }
        if (getTemplateStream() == null) {
            setTemplateStream(new FileInputStream(getDefaultTemplateFile()));
            addMetaData(MetaData.key.template, getDefaultTemplateFile().getAbsolutePath());
        }
        setTemplateText(IOUtils.toString(getTemplateStream()));

        Map allTokens = getTokenMap();
        StrSubstitutor sub = new StrSubstitutor(allTokens);
        String documentText = sub.replace(getTemplateText());
        if (documentText == null) {
            throw new NullPointerException("can't render an invoice whose content is empty");
        }
        getOutputStream().write(documentText.getBytes());
        getOutputStream().close();
    }

    protected void addLocalizedToken(String propertyKey) {
        getTokenMap().put(propertyKey, getLocalizedProperty(propertyKey));
    }

    protected void addDynamicToken(String key, String value) {
        getTokenMap().put(key, value);
    }

    private String getLocalizedProperty(String propertyKey) {
        String value =  getLocalizedProperties().getProperty(propertyKey.toUpperCase());
        if (value == null) {
            throw new NullPointerException("'" + propertyKey.toUpperCase() + "' is missing from resource bundle");
        }
        return getFormatter().escapeText(value);
    }

    public String toUpperCase(InvoiceDocument.Keys k) {
        return k.toString().toUpperCase();
    }

    public abstract void initTokens() throws IOException;

    public abstract File getDefaultTemplateFile()  throws ConfigurationException;

    public abstract File getDefaultOutputFile()  throws ConfigurationException;

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public String getTemplateText() {
        return templateText;
    }

    public void setTemplateText(String templateText) {
        this.templateText = templateText;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public InputStream getTemplateStream() {
        return templateStream;
    }

    public void setTemplateStream(InputStream templateStream) {
        this.templateStream = templateStream;
    }

    public Map getTokenMap() {
        return tokenMap;
    }

    public void setTokenMap(Map tokenMap) {
        this.tokenMap = tokenMap;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Properties getLocalizedProperties() {
        return localizedProperties;
    }

    public void setLocalizedProperties(Properties localizedProperties) {
        this.localizedProperties = localizedProperties;
    }

    public AbstractDocumentFormatter getFormatter() {
        return formatter;
    }

    public void setFormatter(AbstractDocumentFormatter formatter) {
        this.formatter = formatter;
    }

    public static class MetaData {

        enum key {output, template, date, time, timestamp}

        private Map metaData = new HashMap();
        private String creationDate;
        private String creationTime;

        public MetaData() {
            this.creationTime = DateTimeUtil.ISO_TIME_FORMATTER.format(LocalTime.now());
            this.creationDate = DateTimeUtil.ISO_DATE_FORMATTER.format(LocalDate.now());
            addData(key.timestamp, creationDate + " " + creationTime);
        }

        public void addData(key key, String value) {
            this.metaData.put(key, value);
        }

        public String getCreationDate() {
            return creationDate;
        }

        public String getCreationTime() {
            return creationTime;
        }

        public String toString() {
            Set<MetaData.key> keys =  metaData.keySet();
            StringBuilder sb = new StringBuilder("\n");
            for (MetaData.key k: keys) {
                String value = (String)metaData.get(k);
                if (value != null) {
                    sb.append(k).append(": ").append(value).append("\n");
                }
            }
            return sb.toString();
        }
    }

    public enum Format {

        TEX("TEX"),
        TXT("TXT"),
        HTML("HTML");

        Format(String name) {
            this.name = name;
        }

        public String getSuffix() {
            return "." + name.toLowerCase();
        }
        private String name;
    }

}
