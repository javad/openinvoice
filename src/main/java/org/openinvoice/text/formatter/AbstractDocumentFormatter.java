/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.formatter;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.money.format.MoneyAmountStyle;
import org.joda.money.format.MoneyFormatter;
import org.joda.money.format.MoneyFormatterBuilder;
import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.util.ArithmeticUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by jhe on 09/03/2018.
 */
public abstract class AbstractDocumentFormatter {

    enum Tag {cellBegin, cellEnd, rowBegin, rowEnd, cellSep, rowSep, rowRule}
    private AbstractDocument.Format format;
    private boolean formatWithCurrency;

    public static final Map<Tag, String> tags = new HashMap();
    public static final Map<String, String> specialTokens = new HashMap();

    public abstract String formatTableRow(String rowText);
    public abstract String formatTableCell(String cellText);
    public abstract String formatLastTableCell(String cellText);
    public abstract String formatTableCell(Money money);

    public abstract String formatLastTableCell(Money money);

    public String formatEmptyTableCell() {
        return formatTableCell("");
    }

    public String formatTableCellAsMoney(String s, CurrencyUnit currencyUnit) {
        return formatTableCell(Money.of(currencyUnit, ArithmeticUtil.createBigDecimal(s)));
    }

    public  String formatMoney(Money money) {
        if (money == null) {
            throw new NullPointerException("can't print null money");
        }
        return getMoneyFormatter().print(money);
    }

    public MoneyFormatter getMoneyFormatter() {
        MoneyFormatterBuilder mf = new MoneyFormatterBuilder();
        if (formatWithCurrency) {
            mf.appendCurrencyCode().appendLiteral(" ");
        }
        mf.appendAmount(MoneyAmountStyle.LOCALIZED_GROUPING);
        return mf.toFormatter();
    }

    public String escapeText(String text) {
        String newText = text;
        Iterator keys = getSpecialTokens().keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            String value = getSpecialTokens().get(key).toString();
            if (newText.contains(key))     // TODO: avoid Null pointer  Figure out the caller from the log file
                newText = newText.replaceAll(key, value);
        }
        return newText;
    }


    public  Map<Tag, String> getTags() {
        return tags;
    }

    public  Map<String, String> getSpecialTokens() {
        return specialTokens;
    }

    public AbstractDocument.Format getFormat() {
        return format;
    }

    public void setFormat(AbstractDocument.Format format) {
        this.format = format;
    }

    public boolean isFormatWithCurrency() {
        return formatWithCurrency;
    }

    public void setFormatWithCurrency(boolean formatWithCurrency) {
        this.formatWithCurrency = formatWithCurrency;
    }
}
