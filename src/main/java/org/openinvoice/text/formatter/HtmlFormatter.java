/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.formatter;

import org.joda.money.Money;
import org.openinvoice.text.doc.AbstractDocument;

/**
 * Created by jhe on 09/03/2018.
 */
public class HtmlFormatter extends AbstractDocumentFormatter {

    public HtmlFormatter() {
        setFormat(AbstractDocument.Format.HTML);
        getTags().put(Tag.cellBegin, "<td>");
        getTags().put(Tag.cellEnd, "</td>");
        getTags().put(Tag.rowBegin, "<tr>");
        getTags().put(Tag.rowEnd, "</tr>");
    }

    public  String formatTableRow(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(getTags().get(Tag.rowBegin));
        sb.append(s);
        sb.append(getTags().get(Tag.rowEnd));
        return sb.toString();
    }



    public String formatTableCell(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(getTags().get(Tag.cellBegin));
        sb.append(escapeText(s));
        sb.append(getTags().get(Tag.cellEnd));
        return sb.toString();
    }

    @Override
    public String formatLastTableCell(String cellText) {
        return formatTableCell(cellText); // for HTML format, it makes no difference
    }

    @Override
    public String formatTableCell(Money money) {
        return formatTableCell(formatMoney(money));
    }

    @Override
    public String formatLastTableCell(Money money) {
        return formatTableCell(money); // for HTML format, it makes no difference
    }
}
