/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.text.formatter;

import org.joda.money.Money;
import org.openinvoice.text.doc.AbstractDocument;

/**
 * Created by jhe on 09/03/2018.
 */
public class TexFormatter extends AbstractDocumentFormatter {

    public TexFormatter() {
        setFormat(AbstractDocument.Format.TEX);
        tags.put(Tag.cellSep, "&");
        tags.put(Tag.rowSep, "\\\\");
        tags.put(Tag.rowRule, "\\midrule ");
        initSpecialTokens();
    }

    public String formatTableRow(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(tags.get(Tag.rowSep));
        sb.append(tags.get(Tag.rowRule));
        return sb.toString();
    }

    public String formatTableCell(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(escapeText(s));
        sb.append(tags.get(Tag.cellSep));
        return sb.toString();
    }

    @Override
    public String formatLastTableCell(String cellText) {
        return escapeText(cellText);
    }

    @Override
    public String formatTableCell(Money money) {
        return formatTableCell(formatMoney(money));
    }

    @Override
    public String formatLastTableCell(Money money) {
        return escapeText(getMoneyFormatter().print(money));
    }

    public void initSpecialTokens() {
        getSpecialTokens().put("è", "\\\\'{e}");
        getSpecialTokens().put("É", "\\\\'{E}");
        getSpecialTokens().put("&", "\\\\&");
        getSpecialTokens().put("#", "\\\\#");
    }

}
