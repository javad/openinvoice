/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */
package org.openinvoice.console;

import org.openinvoice.entity.InvoiceWrapper;
import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.exception.OpenInvoiceException;

import java.io.*;
import java.sql.SQLException;

/**
 * Created by jhe on 29/12/2017.
 */
public class ConsoleRequest {

    private Console.ARGUMENTS requestName;
    private File inputFile;
    private File outputFile;
    private File templateFile;
    private OutputStream outputStream;
    private InputStream inputStream;
    private InputStream templateStream;
    private InvoiceWrapper invoiceWrapper = new InvoiceWrapper();;

    protected void handle() throws IOException, SQLException, OpenInvoiceException {
        AbstractDocument document = null;
        switch (getRequestName()) {
            case CLONE:
                document = invoiceWrapper.cloneInvoice(getInputStream(), getOutputStream(), getTemplateStream());
                break;
            case RENDER:
                document = invoiceWrapper.renderInvoice(getInputStream(), getOutputStream(), getTemplateStream());
                break;
            case CREATE:
                document = invoiceWrapper.createInvoice(getInputStream(), getOutputStream(), getTemplateStream());
                break;
            case DELETE:
                invoiceWrapper.deleteInvoice(getInputStream());
                break;
            case QUERY:
                document = invoiceWrapper.queryInvoiceRepository(getInputStream(), getOutputStream(), getTemplateStream());
                break;
            case CONFIG:
                Configuration.printConfiguration(System.out);
                break;
            default:
                throw new IOException("unknown request '" + getRequestName().toString() + "'");
        }
        if (document != null) {
            Configuration.consoleLogger.info("Output document information:" + document.getMetaData().toString());
        }
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public InputStream getTemplateStream() {
        return templateStream;
    }

    public void setTemplateStream(InputStream templateStream) {
        this.templateStream = templateStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public ConsoleRequest(Console.ARGUMENTS requestName) {
        this.requestName = requestName;
    }

    public Console.ARGUMENTS getRequestName() {
        return requestName;
    }

    public void setRequestName(Console.ARGUMENTS requestName) {
        this.requestName = requestName;
    }

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

   public File getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(File templateFile) {
        this.templateFile = templateFile;
    }

    public String formatArguments() {
        StringBuilder sb = new StringBuilder();
        if (templateFile != null) {
            sb.append("Template: ").append(templateFile.getPath()).append(" ");
        }
        if (inputFile != null) {
            sb.append("Input: ").append(inputFile.getPath()).append(" ");
        }
        if (outputFile != null) {
            sb.append("Output: ").append(outputFile.getPath());
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Request: ").append(requestName.name()).append(" ");
        sb.append(formatArguments());
        return sb.toString();
    }
}
