/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */
package org.openinvoice.console;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.openinvoice.util.Configuration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhe on 30/12/2017.
 */
public class Console {

    @Option(name="-i", required = true, usage="Specifies the path to the input payload file.", metaVar="Input File Path")
    private File inputFile;

    @Option(name="-o",usage="Specifies the path to the output file.", metaVar="Output File Path")
    private File outputFile;

    @Option(name="-t",usage="Specifies the path to the template file.", metaVar="Template File Path")
    private File templateFile;

    @Argument
    private List<String> arguments = new ArrayList<String>();

    public enum ARGUMENTS {CLONE, RENDER, CREATE, DELETE, QUERY, CONFIG};

    public static void main(String[] args)  {
      new Console().doMain(args);
    }

    private void doMain(String[] args)  {
        CmdLineParser parser = new CmdLineParser(this);
        ConsoleRequest request = null;
        try {
            parser.parseArgument(args);
        } catch( CmdLineException e ) {
            System.err.println(e.getMessage());
        }
        if (arguments.size() != 1) {
            printUsage(parser);
        }
        for (String argument: arguments) {
            if (isValidArgument(argument)) {
                request = new ConsoleRequest(ARGUMENTS.valueOf(argument.toUpperCase()));
            } else {
                Configuration.consoleLogger.error("invalid argument: {}", argument);
                printUsage(parser);
                break;
            }
        }
        // Print configuration
        if (request.getRequestName().toString().equalsIgnoreCase(ARGUMENTS.CONFIG.name())) {
            try {
                Configuration.printConfiguration(System.out);
            } catch (IOException e) {
                Configuration.consoleLogger.error("can't read/print configuration!", e);
                System.exit(ExitCode.ERROR.getCode());
            }
            System.exit(ExitCode.SUCCESS.getCode());
        }

        // Process the request
        if (request != null) {

            // Process the input file
            if (inputFile == null) {
                printUsage(parser);
            }
            try {
                request.setInputFile(inputFile);
                request.setInputStream(new FileInputStream(inputFile));
            } catch (FileNotFoundException e) {
                Configuration.consoleLogger.error("can't find the input file: {}", inputFile.getPath());
                printUsage(parser);
            }

            // Process the output file
            if (outputFile != null) {
                try {
                    request.setOutputFile(outputFile);
                    request.setOutputStream(new FileOutputStream(outputFile));
                } catch (FileNotFoundException e) {
                    Configuration.consoleLogger.error("can't find the output file: {}", outputFile.getPath());
                    printUsage(parser);
                }
            }

            // Process the template file
            if (templateFile != null) {
                try {
                    request.setTemplateFile(templateFile);
                    request.setTemplateStream(new FileInputStream(templateFile));
                } catch (FileNotFoundException e) {
                    Configuration.consoleLogger.error("can't find the template file: {}", templateFile.getPath());
                    printUsage(parser);
                }
            }

            Configuration.consoleLogger.info("Received the {} command. {}" , request.getRequestName(), request.formatArguments());
            try {
               request.handle();
               System.exit(ExitCode.SUCCESS.getCode());
            } catch (Exception e) {
                Configuration.consoleLogger.error(e.getMessage());
                Configuration.globalLogger.error(e.getMessage(), e);
                System.exit(ExitCode.ERROR.getCode());
            }
        }
    }

    private boolean isValidArgument(String arg) {
        ARGUMENTS[] validArgs = ARGUMENTS.values();
        for (ARGUMENTS validArg : validArgs) {
            if (validArg.toString().equalsIgnoreCase(arg))
                return true;
        }
        return false;
    }

    private void printUsage(CmdLineParser parser) {
        StringBuffer usage = new StringBuffer("\n");
        usage.append("USAGE: java -jar ").append(Configuration.PRJ_NAME).append("-").append(Configuration.PRJ_VER).append(".jar [options] ");
        ARGUMENTS[] arguments = ARGUMENTS.values();
        for (int i=0; i < arguments.length; i++) {
            usage.append(arguments[i].toString().toLowerCase());
            if (i < arguments.length - 1)
                usage.append("|");
        }
        usage.append("\n\n Where options are:\n");
        System.out.println(usage);
        parser.printUsage(System.out);
        System.out.println("\nOnline documentation available at http://www.openinvoice.org");
        System.exit(ExitCode.ERROR.getCode());
    }

    private enum ExitCode {
        SUCCESS("SUCCESS",0),
        ERROR("ERROR",1);
        private final String name;
        private final int code;

        ExitCode(String name, int code) {
            this.name = name;
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public int getCode() {
            return code;
        }

        public String toString() {
            return String.valueOf(code);
        }
    }
}
