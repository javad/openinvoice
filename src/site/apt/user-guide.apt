Installation

    To install Open Invoice:

    [[1]] Download the Open Invoice tarball file using either a browser or a tool such as wget or scp.

     <<<wget https://bitbucket.org/javad/openinvoice/downloads/openinvoice-<version>-bin.tar.gz>>>

    [[2]] Extract the files from the compressed tarball into a directory of your choice.

    <<<tar xvfz openinvoice-<version>-bin.tar.gz>>>

    [[3]] Test the installtion

    <<<cd openinvoice-<version> >>>

    <<<java -jar lib/openinvoice-<version>.jar>>>

    []

    Open Invoice reads the <<<.openinvoice.properties>>> configuration file from the user home directory to initialize
    its configuration information.

    Open Invoice runs on {{{https://java.com/en/} Java}} 8 or greater. Make sure that the Java runtime
    is properly installed.

Usage

    The Open Invoice is used via its commandline interface:

    <<<java -jar lib/openinvoice-<version>.jar [option] action>>>

* Actions

** <<<CREATE>>>

    Creates a new invoice based on the input order file. In the following example, the <<<-i>>> is mandatory, it specifies the full path to the order.xml.
    The <<<-o>>> option specifies the full path to the output file. The -t option specifies the full path to the template file used to render the invoice output.
    In absence of the  <<<-o>>> option, the default invoice name is used.  In absence of the  <<<-t>>> option, the default invoice template is used to render
    the invoice output. See the <<<.openinvoice.properties>>> configuration file for more information about the default configuration parameters.

    <<<$ java -jar lib/openinvoice-<version>.jar -i order.xml -t template.html -o my-invoice-document.tex CREATE>>>

** <<<RENDER>>>

    Renders an an existing invoice to the desired output format. Supported formats include <<<HTML>>>, <<<TeX>>> and <<<TEXT>>>.

    <<<$ java -jar lib/openinvoice-<version>.jar -i render-request.xml -o invoice.tex RENDER>>>

** <<<CLONE>>>

    Clones a new invoice based on an existing invoice.

    <<<$ java -jar lib/openinvoice-<version>.jar -i clone-request.xml -o invoice.txt CLONE>>>

** <<<DELETE>>>

    Deletes an existing invoice from the repository.

    <<<$ java -jar lib/openinvoice-<version>.jar -i delete-request.xml DELETE>>>

* Options

** <<<-i (Input)>>>

    Specifies the full path to the input payload file. The <<<CREATE>>> action requires an input order file.

** <<<-o (Output)>>>

    Specifies the full path to the output file. If not specified, the default output path is used.
    The default output directory is specified in the <<<.openinvoice.properties>>> configuration
    file. The default output file name is composed of the <invoice issue date>, the <invoice number>
    and the <invoice customer name>.

** <<<-t (Template)>>>

    Specifies the full path to the invoice template file used to produce the output. If not specified,
    the default template corresponding to the output format is selected.

