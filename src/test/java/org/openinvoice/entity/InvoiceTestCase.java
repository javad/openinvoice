/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.Money;
import org.junit.Assert;
import org.openinvoice.text.doc.InvoiceDocument;

import java.time.LocalDate;

public class InvoiceTestCase {

    private String expectedInvoiceNumber;
    private Money expectedTotalAmount;
    private Money expectedSubtotalAmount;
    private Money expectedTaxAmount;
    private LocalDate expectedIssueDate;
    private LocalDate expectedDueDate;

    String getExpectedInvoiceNumber() {
        return expectedInvoiceNumber;
    }

    void setExpectedInvoiceNumber(String expectedInvoiceNumber) {
        this.expectedInvoiceNumber = expectedInvoiceNumber;
    }

    public Money getExpectedTotalAmount() {
        return expectedTotalAmount;
    }

    void setExpectedTotalAmount(Money expectedTotalAmount) {
        this.expectedTotalAmount = expectedTotalAmount;
    }

    public Money getExpectedSubtotalAmount() {
        return expectedSubtotalAmount;
    }

    void setExpectedSubtotalAmount(Money expectedSubtotalAmount) {
        this.expectedSubtotalAmount = expectedSubtotalAmount;
    }

    public Money getExpectedTaxAmount() {
        return expectedTaxAmount;
    }

    void setExpectedTaxAmount(Money expectedTaxAmount) {
        this.expectedTaxAmount = expectedTaxAmount;
    }

    public LocalDate getExpectedIssueDate() {
        return expectedIssueDate;
    }

    void setExpectedIssueDate(LocalDate expectedIssueDate) {
        this.expectedIssueDate = expectedIssueDate;
    }

    public LocalDate getExpectedDueDate() {
        return expectedDueDate;
    }

    void setExpectedDueDate(LocalDate expectedDueDate) {
        this.expectedDueDate = expectedDueDate;
    }

    public void testInvoice(InvoiceDocument invoiceDocument) throws Exception {
        Assert.assertNotNull(invoiceDocument);
        Invoice invoice = invoiceDocument.getInvoice();
        Assert.assertNotNull(invoice);
        Assert.assertEquals(expectedInvoiceNumber, invoice.getInvoiceNumber());
        Assert.assertEquals(expectedIssueDate, invoice.getIssueDate());
        Assert.assertEquals(expectedDueDate, invoice.getDueDate());
        Assert.assertEquals(expectedTotalAmount, invoice.getTotalAmount());
        Assert.assertEquals(expectedSubtotalAmount, invoice.getSubtotalAmount());
        Assert.assertEquals(expectedTaxAmount, invoice.getTaxAmount());
    }

}
