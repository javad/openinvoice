/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.CurrencyUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openinvoice.text.doc.InvoiceDocument;
import org.openinvoice.util.ArithmeticUtil;
import org.openinvoice.util.DateTimeUtil;

/**
 * Created by jhe on 27/02/2018.
 */
public class InvoiceWrapperCreateMultipleItemsTest extends InvoiceTestCase {

    private InvoiceWrapper invoiceWrapper;
    private TestCaseConfiguration testCaseConfiguration;

    @Before
    public void setUp() throws Exception {

        this.testCaseConfiguration = new TestCaseConfiguration(TestCaseConfiguration.TestCase.CreateMultipleItems);
        this.invoiceWrapper = new InvoiceWrapper();

        setExpectedInvoiceNumber("100001");
        setExpectedTotalAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "11058.16"));
        setExpectedSubtotalAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "10848.16"));
        setExpectedTaxAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "210.00"));
        setExpectedIssueDate(DateTimeUtil.parse("2060-12-31"));
        setExpectedDueDate(DateTimeUtil.parse("2061-01-31"));
    }

    @Test
    public void testCreateInvoice() throws Exception {
        InvoiceDocument invoiceDocument = (InvoiceDocument) invoiceWrapper.createInvoice(testCaseConfiguration.getInputStream(), testCaseConfiguration.getOutputStream(), null);
        super.testInvoice(invoiceDocument);
    }

    @After
    public void tearDown() throws Exception {
        Invoice.deleteByInvoiceNumber(getExpectedInvoiceNumber());
        testCaseConfiguration.tearDown();
    }

}