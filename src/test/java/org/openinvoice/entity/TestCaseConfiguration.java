/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.openinvoice.text.doc.AbstractDocument;
import org.openinvoice.util.Configuration;
import org.openinvoice.util.DatabaseUtil;
import org.openinvoice.util.exception.ConfigurationException;

import java.io.*;
import java.sql.Connection;

public class TestCaseConfiguration {

    private static final String INPUT_PATH = "files/";
    private static final String DB_PATH =    "src/test/resources/database/";

    private File outputFile;
    private Connection dbConnection;
    private TestCase testCase;

     public TestCaseConfiguration(TestCase testCase) throws ConfigurationException {
        this.testCase = testCase;
        initDatabaseConnection();
    }

    private void initDatabaseConnection() throws ConfigurationException {
        String databaseFilePath = DB_PATH + testCase.getDatabaseName();
        this.dbConnection = DatabaseUtil.getDatabaseConnection(databaseFilePath);
        Configuration.setDatabaseConnection(this.dbConnection);
    }

    protected InputStream getInputStream() {
        String inputFilePath = INPUT_PATH + testCase.getFileName();
       return TestCaseConfiguration.class.getClassLoader().getResourceAsStream(inputFilePath);
    }

    public OutputStream getOutputStream() throws IOException {
        String prefix = Configuration.PRJ_NAME + "_" + this.testCase.name();
        String suffix = AbstractDocument.Format.TXT.getSuffix();
        this.outputFile = File.createTempFile(prefix, suffix);
        return new FileOutputStream(this.outputFile);
    }

    public void tearDown() throws Exception {
        outputFile.delete();
        if (dbConnection != null) {
            try {
                this.dbConnection.close();
            } finally {
                this.dbConnection  = null;
            }
        }
    }

    public TestCase getTestCase() {
        return testCase;
    }

    enum TestCase {

        CreateMultipleItems("create", "multiple_items"),
        CloneBasedOnInput("clone", "based_on_input"),
        CloneBasedOnLatest("clone", "based_on_latest"),
        RenderTex("render", "tex"),
        RenderHTML("render", "html");

        private final String testCategory;
        private final String testSubCategory;

        TestCase(String testCategory, String testSubCategory) {
            this.testCategory = testCategory;
            this.testSubCategory = testSubCategory;
        }

        public String getDatabaseName() {
            return testCategory + ".db";
        }

        public String getTestCategory() {
            return testCategory;
        }

        public String getTestSubCategory() {
            return testSubCategory;
        }

        public String getFileName() {
            StringBuilder fileName = new StringBuilder(testCategory);
            if (!testSubCategory.isEmpty()) {
                fileName.append("_").append(testSubCategory);
            }
            fileName.append(".xml");
            return fileName.toString();
        }
    }
}
