/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.joda.money.CurrencyUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openinvoice.text.doc.InvoiceDocument;
import org.openinvoice.util.*;

/**
 * Created by jhe on 27/02/2018.
 */
public class InvoiceWrapperCloneBasedOnInputTest extends InvoiceTestCase {

    private TestCaseConfiguration testCaseConfiguration;
    private InvoiceWrapper invoiceWrapper;

    @Before
    public void setUp() throws Exception {
        this.testCaseConfiguration = new TestCaseConfiguration(TestCaseConfiguration.TestCase.CloneBasedOnInput);
        this.invoiceWrapper = new InvoiceWrapper();

        setExpectedInvoiceNumber("100004");
        setExpectedTotalAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "44905.00"));
        setExpectedSubtotalAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "39655.00"));
        setExpectedTaxAmount(ArithmeticUtil.createMoney(CurrencyUnit.CHF, "5250.00"));
        setExpectedIssueDate(DateTimeUtil.parse("2061-02-28"));
        setExpectedDueDate(DateTimeUtil.parse("2061-03-08"));
    }

    @Test
    public void testCloneInvoice() throws Exception {
        InvoiceDocument invoiceDocument = (InvoiceDocument) invoiceWrapper.cloneInvoice(testCaseConfiguration.getInputStream(), testCaseConfiguration.getOutputStream(), null);
        super.testInvoice(invoiceDocument);
    }

    @After
    public void tearDown() throws Exception {
        Invoice.deleteByInvoiceNumber(getExpectedInvoiceNumber());
        testCaseConfiguration.tearDown();
    }

}