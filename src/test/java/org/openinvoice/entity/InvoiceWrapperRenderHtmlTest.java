/*
 * Copyright (c) 2018. Plain Source S.P.R.L.
 * GNU GPL, see <http://www.gnu.org/licenses/>
 */

package org.openinvoice.entity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openinvoice.text.doc.InvoiceDocument;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by jhe on 27/02/2018.
 */
public class InvoiceWrapperRenderHtmlTest {

    private TestCaseConfiguration testCaseConfiguration;
    private InvoiceWrapper invoiceWrapper;

    @Before
    public void setUp() throws Exception {
        this.testCaseConfiguration = new TestCaseConfiguration(TestCaseConfiguration.TestCase.RenderHTML);
        this.invoiceWrapper = new InvoiceWrapper();
    }

    @Test
    public void testRenderInvoice() throws Exception {
        InputStream inputPayload = testCaseConfiguration.getInputStream();
        OutputStream outputStream = testCaseConfiguration.getOutputStream();
        InvoiceDocument invoiceDocument = (InvoiceDocument) invoiceWrapper.renderInvoice(inputPayload, outputStream, null);
        Assert.assertEquals(invoiceDocument.getFormat(), InvoiceDocument.Format.HTML);
    }

    @After
    public void tearDown() throws Exception {
       testCaseConfiguration.tearDown();
    }

}