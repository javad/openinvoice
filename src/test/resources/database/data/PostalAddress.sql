INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Belgium', 'Kraainem', 'Avenue Des Perdrix', '17', '1950');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Belgium', 'Brussels', 'Charles Thielemans', '63', '1150');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Switzerland', 'Zoug', 'Baarerstrasse', '75', 'CH-6300');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Belgium', 'Genval', 'Rue de Rosières ', '91', '1332');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Belgium', 'Zele', 'Industriestraat', '66A', '9240');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Switzerland', 'Frauenfeld ', 'Juchstrasse', '44', 'CH-8500');
INSERT INTO PostalAddress (country, city, street, buildingNumber, postalCode) VALUES ('Belgium', 'Kraainem', 'Avenue Des Grives', '5', '1950');