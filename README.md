#Description

Open Invoice is an invoice management application aiming to simplify the invoicing process for freelancers.
It is implemented in Java and uses the common vocabulary defined by the Universal Business Language (UBL).

Form more information, visit the  [Open Invoice website](http://openinvoice.org).

#Installation

1. Download the Open Invoice tarball file using either a browser or a tool such as wget or scp.
    `$ wget https://bitbucket.org/javad/openinvoice/downloads/openinvoice-<version>-bin.tar.gz`

2. Extract the files from the compressed tarball into a directory of your choice
    `$ tar xvfz openinvoice-<version>-bin.tar.gz`

3. Test the installtion
    `$ cd openinvoice-<version>`
    `$ java -jar lib/openinvoice-<version>.jar`

Open Invoice reads the `.openinvoice.properties` configuration file from the user home directory to initialize
its configuration information.

#Usage

Given an order, it creates an invoice and renders its content to TeX output.
The '-i' option is used to specify the order input file path. The '-o' option is
used to specify the invoice output file path.

`$ java -jar openinvoice-<version>.jar -i order.xml -o invoice.tex CREATE`